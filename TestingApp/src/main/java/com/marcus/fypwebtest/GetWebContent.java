package com.marcus.fypwebtest;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Marcus on 1/19/2016.
 */
public class GetWebContent extends AsyncTask<String, Void, String> {


    @Override
    protected String doInBackground(String... strings) {
        String res = "";
        try {
            URL url = new URL(strings[0]);
            Log.d("t",strings[0]);
            Document doc = Jsoup.parse(url, 3000);
            Elements divs;
            if(strings[0].contains("google"))
                divs = doc.select(".kno-rdesc span");
            else
                divs = doc.select("#bodyContent p");
            if(divs.size()>0) { //found
                int i = 0;
                do {
                    Element div = divs.get(i++);
                    res = div.text();
                } while (res.contains("HIDDEN ERROR"));
            }
            return res;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
