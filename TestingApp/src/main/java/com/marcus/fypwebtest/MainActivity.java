package com.marcus.fypwebtest;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    EditText name,result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD)
        {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
        }
        Button btn = (Button) findViewById(R.id.button);
        name = (EditText) findViewById(R.id.name);
        name.setText("David Archuleta");
        result = (EditText) findViewById(R.id.result);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String g = getGoogleA(name.getText().toString());
                String w = getWikiA(name.getText().toString());
                result.setText("Google : \n" + g + "\n\n Wiki : \n" + w);
            }
        });
    }

    public String getGoogleA(String s){
        String r="";
        s = s.replaceAll(" ","+");
        String web = "https://www.google.co.uk/search?q="+s;
        GetWebContent t = new GetWebContent();
        t.execute(web);
        try {
            r = t.get();
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return r;
    }

    public String getWikiA(String s){
        String r="";
        //s = s.replaceAll(" ","+");
        String web = "https://en.m.wikipedia.org/wiki/"+s;
        GetWebContent t = new GetWebContent();
        t.execute(web);
        try {
            r = t.get();
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return description(r);
    }

    public String getGoogle(String s){
        s = s.replaceAll(" ","+");
        String web = "https://www.google.co.uk/search?q="+s;
        try {
            URL url = new URL(web);
            Document doc = Jsoup.parse(url, 3000);
            Elements divs = doc.select(".kno-rdesc span");
                Element div = divs.get(0);
                String res = div.text();
            //result .setText(description(res));
            return res;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getWiki(String s){
        String web = "https://en.m.wikipedia.org/wiki/"+s;
        try {
            URL url = new URL(web);
            Document doc = Jsoup.parse(url, 3000);
            Elements divs = doc.select("#bodyContent p");
            int i = 0;
            String res="";
            do {
                Element div = divs.get(i++);
                res = div.text();
            }while(res.contains("HIDDEN ERROR"));
            //result .setText(description(res));
            return description(res);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String description(String s){
        String r;
        r = s.replaceAll("\\[\\d+\\]", "");
        String[] a = r.split(" ");
        if(a.length>80) {
            r="";
            for(int i=0;i<80;i++){
                r+=a[i]+" ";
            }
            r+="...";
        }
        return r;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
