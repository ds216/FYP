<?
if($_GET[r])
	$GLOBALS[r] = $_GET[r];
else
	$GLOBALS[r] = 0.125;
	
if($_GET[c])
	$GLOBALS[c] = $_GET[c];
else
	$GLOBALS[c] = 4;
?>
<form method="get">
<p>R : <input type="number" name="r" value ="<?=$GLOBALS[r]?>" step="0.001"/>; C : <input type="number" name="c" value = "<?=$GLOBALS[c]?>"/><input type="submit" /></p>
</form>
<p>Algorithm : checking each landmarks with the reference data (a average position of the landmark), if the distance between two landmarks is < <?=$GLOBALS[r]?>, it will marked as "MATCH", if the number of "match landmark" >= <?=$_GLOBAL[c]?>, face will marked as "MATCH".</p>
<?
$re = "/.*#(.*):.*#(.*) \\[(.*)].*Rx = (.*),\\tRy = (.*)/"; 
$str = "

Face #0: Landmark #1 [RIGHT_EYE]	: Rx = 0.799858175,	Ry = 1.1451167
Face #0: Landmark #2 [LEFT_EYE]	: Rx = -0.855778493333,	Ry = 1.14511663333
Face #0: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #0: Landmark #4 [LEFT_CHEEK]	: Rx = -1.03917311667,	Ry = 0.243996898333
Face #0: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.89966499,	Ry = 0.235234815
Face #0: Landmark #6 [LEFT_MOUTH]	: Rx = -0.789865726667,	Ry = -0.581637658333
Face #0: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.636231758333,	Ry = -0.654178918333
Face #0: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.0367691363333,	Ry = -0.965118963333


Face #1: Landmark #1 [RIGHT_EYE]	: Rx = 0.90046364,	Ry = 1.1721101
Face #1: Landmark #2 [LEFT_EYE]	: Rx = -0.8025654,	Ry = 1.1721101
Face #1: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #1: Landmark #4 [LEFT_CHEEK]	: Rx = -0.9758962,	Ry = 0.2805365
Face #1: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.9521021,	Ry = 0.25080374
Face #1: Landmark #6 [LEFT_MOUTH]	: Rx = -0.72103363,	Ry = -0.5306261
Face #1: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.6600867,	Ry = -0.62195474
Face #1: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.034576785,	Ry = -0.9519186

Face #2: Landmark #1 [RIGHT_EYE]	: Rx = 0.75176036,	Ry = 1.2005156
Face #2: Landmark #2 [LEFT_EYE]	: Rx = -0.8756852,	Ry = 1.2005152
Face #2: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #2: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0064436,	Ry = 0.2120636
Face #2: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.9235784,	Ry = 0.30121902
Face #2: Landmark #6 [LEFT_MOUTH]	: Rx = -0.78138256,	Ry = -0.4755172
Face #2: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.8009276,	Ry = -0.43659148
Face #2: Landmark #8 [BOTTOM_MOUTH]	: Rx = 0.21335216,	Ry = -0.9287672

Face #3: Landmark #1 [RIGHT_EYE]	: Rx = 0.7703229,	Ry = 1.1129755
Face #3: Landmark #2 [LEFT_EYE]	: Rx = -0.91429996,	Ry = 1.1129755
Face #3: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #3: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0937828,	Ry = 0.21350326
Face #3: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.87736374,	Ry = 0.12031107
Face #3: Landmark #6 [LEFT_MOUTH]	: Rx = -0.8267749,	Ry = -0.6446897
Face #3: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.5747069,	Ry = -0.77723366
Face #3: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.12796044,	Ry = -0.99521095

[May Not Work]
Face #4: Landmark #1 [RIGHT_EYE]	: Rx = 0.7470322,	Ry = 1.1434975
Face #4: Landmark #2 [LEFT_EYE]	: Rx = -0.9225342,	Ry = 1.1434975
Face #4: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #4: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0705898,	Ry = 0.19166137
Face #4: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.8744755,	Ry = 0.2602829
Face #4: Landmark #6 [LEFT_MOUTH]	: Rx = -0.8273392,	Ry = -0.680655
Face #4: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.6073708,	Ry = -0.71900713
Face #4: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.09158967,	Ry = -1.0495536

Face #5: Landmark #1 [RIGHT_EYE]	: Rx = 0.7715441,	Ry = 1.136902
Face #5: Landmark #2 [LEFT_EYE]	: Rx = -0.8111152,	Ry = 1.136902
Face #5: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #5: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0527503,	Ry = 0.2361549
Face #5: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.8863998,	Ry = 0.25039434
Face #5: Landmark #6 [LEFT_MOUTH]	: Rx = -0.76640403,	Ry = -0.6145224
Face #5: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.608085,	Ry = -0.6380993
Face #5: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.028574103,	Ry = -0.93097043

Face #6: Landmark #1 [RIGHT_EYE]	: Rx = 0.85802585,	Ry = 1.1046995
Face #6: Landmark #2 [LEFT_EYE]	: Rx = -0.808471,	Ry = 1.1046995
Face #6: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #6: Landmark #4 [LEFT_CHEEK]	: Rx = -1.035576,	Ry = 0.33006176
Face #6: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.8840704,	Ry = 0.22839782
Face #6: Landmark #6 [LEFT_MOUTH]	: Rx = -0.81626004,	Ry = -0.54381555
Face #6: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.56621355,	Ry = -0.7321872
Face #6: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.15126598,	Ry = -0.934293

Face B
Face #7: Landmark #1 [RIGHT_EYE]	: Rx = 1.0452732,	Ry = 1.1357967
Face #7: Landmark #2 [LEFT_EYE]	: Rx = -0.68403816,	Ry = 1.1357967
Face #7: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #7: Landmark #4 [LEFT_CHEEK]	: Rx = -0.9067455,	Ry = 0.20574746
Face #7: Landmark #5 [RIGHT_CHEEK]	: Rx = 1.0542172,	Ry = 0.18429355
Face #7: Landmark #6 [LEFT_MOUTH]	: Rx = -0.73050195,	Ry = -0.589165
Face #7: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.68671834,	Ry = -0.63157564
Face #7: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.11389963,	Ry = -0.9696589

[Same as Face#7]
Face #11: Landmark #1 [RIGHT_EYE]	: Rx = 1.0529685,	Ry = 1.133031
Face #11: Landmark #2 [LEFT_EYE]	: Rx = -0.72418797,	Ry = 1.133031
Face #11: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #11: Landmark #4 [LEFT_CHEEK]	: Rx = -0.883022,	Ry = 0.1456498
Face #11: Landmark #5 [RIGHT_CHEEK]	: Rx = 1.0771747,	Ry = 0.24662216
Face #11: Landmark #6 [LEFT_MOUTH]	: Rx = -0.67099726,	Ry = -0.6804705
Face #11: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.771175,	Ry = -0.70501745
Face #11: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.05303627,	Ry = -0.9936206

Face C
Face #8: Landmark #1 [RIGHT_EYE]	: Rx = 0.82009083,	Ry = 1.1187152
Face #8: Landmark #2 [LEFT_EYE]	: Rx = -0.8622726,	Ry = 1.1187152
Face #8: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #8: Landmark #4 [LEFT_CHEEK]	: Rx = -0.9357582,	Ry = 0.28110772
Face #8: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.9714392,	Ry = 0.3204569
Face #8: Landmark #6 [LEFT_MOUTH]	: Rx = -0.7033403,	Ry = -0.6150526
Face #8: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.7875851,	Ry = -0.5490364
Face #8: Landmark #8 [BOTTOM_MOUTH]	: Rx = 0.06898427,	Ry = -0.9702447

Face #9: Landmark #1 [RIGHT_EYE]	: Rx = 0.7819295,	Ry = 1.0948257
Face #9: Landmark #2 [LEFT_EYE]	: Rx = -0.8692462,	Ry = 1.0948257
Face #9: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #9: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0106453,	Ry = 0.17993458
Face #9: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.9583342,	Ry = 0.17094874
Face #9: Landmark #6 [LEFT_MOUTH]	: Rx = -0.7158169,	Ry = -0.58487386
Face #9: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.7337205,	Ry = -0.6183591
Face #9: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.012923302,	Ry = -0.92509085

Face #10: Landmark #1 [RIGHT_EYE]	: Rx = 0.8575305,	Ry = 1.16738
Face #10: Landmark #2 [LEFT_EYE]	: Rx = -0.8302476,	Ry = 1.16738
Face #10: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #10: Landmark #4 [LEFT_CHEEK]	: Rx = -0.9261378,	Ry = 0.26272658
Face #10: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.98607725,	Ry = 0.32199457
Face #10: Landmark #6 [LEFT_MOUTH]	: Rx = -0.62979543,	Ry = -0.626587
Face #10: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.7141139,	Ry = -0.5801555
Face #10: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.017853336,	Ry = -0.9760277

Face #12: Landmark #1 [RIGHT_EYE]	: Rx = 0.8676439,	Ry = 1.2520735
Face #12: Landmark #2 [LEFT_EYE]	: Rx = -0.81643844,	Ry = 1.2520735
Face #12: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
Face #12: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0389687,	Ry = 0.27552912
Face #12: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.89453727,	Ry = 0.2358932
Face #12: Landmark #6 [LEFT_MOUTH]	: Rx = -0.8353298,	Ry = -0.5919477
Face #12: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.5378201,	Ry = -0.69167006
Face #12: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.15546347,	Ry = -1.1331642

02-03 03:40:42.079 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #1 [RIGHT_EYE]	: Rx = 0.869509,	Ry = 1.0155865
02-03 03:40:42.079 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #2 [LEFT_EYE]	: Rx = -0.8591772,	Ry = 1.0155865
02-03 03:40:42.079 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #3 [NOSE_BASE]	: Rx = 0.0,	Ry = 0.0
02-03 03:40:42.079 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #4 [LEFT_CHEEK]	: Rx = -1.0132034,	Ry = 0.25632262
02-03 03:40:42.079 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #5 [RIGHT_CHEEK]	: Rx = 0.925439,	Ry = 0.23527144
02-03 03:40:42.079 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #6 [LEFT_MOUTH]	: Rx = -0.8492304,	Ry = -0.51837194
02-03 03:40:42.080 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #7 [RIGHT_MOUTH]	: Rx = 0.6631044,	Ry = -0.56284165
02-03 03:40:42.080 15375-15375/com.google.android.gms.samples.vision.face.photo D/Face #13: Landmark #8 [BOTTOM_MOUTH]	: Rx = -0.06550257,	Ry = -0.905898"; 
$expected = array(1,1,1,1,1,1,1,0,0,1,0,0,1,1);
$faces = array();
$res = array();
$avg = array();
preg_match_all($re, $str, $matches);
$matches = turn_array($matches);
foreach($matches as $matche){
	$arr = array();
	$arr[]=$matche[3];
	$arr[]=$matche[4];
	$arr[]=$matche[5];
	$faces[$matche[1]][$matche[2]] = $arr;
	
	//echo "Face #{$matche[1]}: Landmark #{$matche[2]} [{$matche[3]}]	: Rx = {$matche[4]},	Ry = {$matche[5]}<br />";
}

for($i=0;$i<count($faces);$i++){
	$res[$i]=0;
	echo "<p>#Face{$i}<table border=\"1\" style=\"border-width:1px; border-color:#000; text-align:center\" cellspacing=\"0px\">\n";
	echo "<tr>
	<td style=\"width:150px\">LANDMARK</td>
	<td style=\"width:150px\">X</td>
	<td style=\"width:150px\">Y</td>
	<td style=\"width:150px\">X<sub>n</sub>-X<sub>0</sub></td>
	<td style=\"width:150px\">X<sub>n</sub>-X<sub>0</sub> (%)</td>
	<td style=\"width:150px\">Y<sub>n</sub>-Y<sub>0</sub></td>
	<td style=\"width:150px\">Y<sub>n</sub>-Y<sub>0</sub> (%)</td>
	<td style=\"width:150px\">diff</td>
	<td style=\"width:150px\">STATUS</td>
	</tr>\n";
	for($j=1;$j<=count($faces[$i]);$j++){
		if($j!=3){
		echo "<tr>\n
		<td>{$faces[$i][$j][0]}</td>\n
		<td>{$faces[$i][$j][1]}</td>\n
		<td>{$faces[$i][$j][2]}</td>\n
		<td>".cal($faces[$i][$j][1],$faces[0][$j][1])."</td>\n
		<td>".cal2($faces[$i][$j][1],$faces[0][$j][1])."</td>\n
		<td>".cal($faces[$i][$j][2],$faces[0][$j][2])."</td>\n
		<td>".cal2($faces[$i][$j][2],$faces[0][$j][2])."</td>\n
		<td>".ddiff($faces[$i][$j][1],$faces[$i][$j][2],$faces[0][$j][1],$faces[0][$j][2])."</td>\n
		<td style=\"background-color:#000\">".pass(ddiff($faces[$i][$j][1],$faces[$i][$j][2],$faces[0][$j][1],$faces[0][$j][2]),$i)."</td></tr>\n";
		if($i<6){
			$avg[$j][0] = $faces[$i][$j][0];
			$avg[$j][1] += $faces[$i][$j][1];
			$avg[$j][2] += $faces[$i][$j][2];
		}}
	}
	$expectedt = ($expected[$i]==1)?"<span style=\"background-color:#000;color:#0F0\">MATCH</span>":"<span style=\"background-color:#000;color:#F00\">NOT MATCH</span>";
	echo "</table>Result : {$res[$i]} / 7 (".passs($res[$i]).") Expected : {$expectedt}</p>\n";
}
echo"<!--";
print_r($avg);
echo "<p>#Avg Face<table border=\"1\" style=\"border-width:1px; border-color:#000; text-align:center\" cellspacing=\"0px\">\n";
	echo "<tr>
	<td style=\"width:150px\">LANDMARK</td>
	<td style=\"width:150px\">X</td>
	<td style=\"width:150px\">Y</td>
	</tr>\n";
foreach($avg as $lm){	
$i = 1;
		echo "<tr>
		<td>".($lm[0])."</td>
		<td>".($lm[1]/6)."</td>
		<td>".($lm[2]/6)."</td>
		</tr>";	
		echo "Face #0: Landmark #{$i} [{$lm[0]}]	: Rx = ".($lm[1]/6).",	Ry = ".($lm[2]/6)."<br />";
	
}
echo "</table>\n";	

print_r($res);

print_r($faces);

function cal($i,$j){
	$diff = abs($i - $j);	
	if($j == 0){
		$par = 0;
	}else{
		$par = round($diff / $j *100,2);
	}
	//return "{$diff} ({$par}%)";
	return "{$diff}";
}
function cal2($i,$j){
	$diff = abs($i - $j);	
	if($j == 0){
		$par = 0;
	}else{
		$par = round($diff / $j *100,2);
	}
	return "{$par}%";
}
function turn_array($m) 
{ 
    for ($z = 0;$z < count($m);$z++) 
    { 
        for ($x = 0;$x < count($m[$z]);$x++) 
        { 
            $rt[$x][$z] = $m[$z][$x]; 
        } 
    }    
    
    return $rt; 
}
function ddiff($x,$y,$x0,$y0){
	return sqrt(($x-$x0)*($x-$x0) + ($y-$y0)*($y-$y0));
}
function pass($n,$i){
	global $res;
	$r = $GLOBALS[r];
	if($n<=$r){
		$res[$i]=$res[$i]+1;
		return "<span style=\"color:#0F0\">MATCH</span>";
		
	}else
		return "<span style=\"color:#F00\">NOT MATCH</span>";
}
function passs($n){
	$r = 4;
	if($n >= $r)	
	return "<span style=\"background-color:#000;color:#0F0\">MATCH</span>";
		
	else
		return "<span style=\"background-color:#000;color:#F00\">NOT MATCH</span>";
	
}
?>