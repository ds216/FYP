package com.google.android.gms.samples.vision.face.photo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;
import java.util.Iterator;
import java.util.List;

public class FaceView
        extends View
{
    private Bitmap mBitmap;
    private SparseArray<Face> mFaces;

    public FaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private double drawBitmap(Canvas canvas) {
        double viewWidth = canvas.getWidth();
        double viewHeight = canvas.getHeight();
        double imageWidth = mBitmap.getWidth();
        double imageHeight = mBitmap.getHeight();
        double scale = Math.min(viewWidth / imageWidth, viewHeight / imageHeight);

        Rect destBounds = new Rect(0, 0, (int)(imageWidth * scale), (int)(imageHeight * scale));
        canvas.drawBitmap(mBitmap, null, destBounds, null);
        return scale;
    }

    private void drawFaceAnnotations(Canvas canvas, double scale)
    {
        Paint greenPaint1 = new Paint();
        Paint redPaint = new Paint();
        Paint bluePaint = new Paint();
        greenPaint1.setColor(Color.GREEN);
        greenPaint1.setStyle(Paint.Style.STROKE);
        greenPaint1.setStrokeWidth(5.0F);
        redPaint.setColor(Color.RED);
        bluePaint.setColor(Color.BLUE);

        for (int i = 0; i < mFaces.size(); ++i) {
            Face face = mFaces.valueAt(i);
            float faceHeight = face.getHeight();
            float faceWidth = face.getWidth();
            PointF facePosition = face.getPosition();
            canvas.drawRect((float) (facePosition.x * scale), (float) (facePosition.y * scale), (float) ((facePosition.x + faceWidth) * scale), (float) ((facePosition.y + faceHeight) * scale), greenPaint1);
            greenPaint1.setTextSize(32.0F);
            canvas.drawText("Face" + i, (float) (facePosition.x * scale), (float) (facePosition.y * scale), greenPaint1);
            facePosition = new PointF();
            facePosition.x = (face.getPosition().x + face.getWidth() / 2.0F);
            facePosition.y = (face.getPosition().y + face.getHeight() / 2.0F);
            canvas.drawCircle((float) (facePosition.x * scale), (float) (facePosition.y * scale), 10.0F, redPaint);

            for (Landmark landmark : face.getLandmarks()) {
                int cx = (int) (landmark.getPosition().x * scale);
                int cy = (int) (landmark.getPosition().y * scale);
                canvas.drawCircle(cx, cy, 10, bluePaint);
            }
/*
            for (Landmark landmark : face.getLandmarks()) {
                PointF pos = landmark.getPosition();
                PointF rotated = rotate_point(facePosition,face.getEulerZ(),pos);
                int rx = (int) (rotated.x * scale);
                int ry = (int) (rotated.y * scale);
                canvas.drawCircle(rx, ry, 10, greenPaint1);
            }*/
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if ((mBitmap != null) && (mFaces != null)) {
            double scale = drawBitmap(canvas);
            drawFaceAnnotations(canvas, scale);
        }
    }

    PointF rotate_point(PointF paramPointF1, float paramFloat, PointF paramPointF2)
    {
        double d = Math.toRadians(paramFloat);
        paramFloat = (float)Math.sin(d);
        float f1 = (float)Math.cos(d);
        paramPointF2.x -= paramPointF1.x;
        paramPointF2.y -= paramPointF1.y;
        float f2 = paramPointF2.x;
        float f3 = paramPointF2.y;
        float f4 = paramPointF2.x;
        float f5 = paramPointF2.y;
        paramPointF1.x += f2 * f1 - f3 * paramFloat;
        paramPointF1.y += f4 * paramFloat + f5 * f1;
        return paramPointF2;
    }

    void setContent(Bitmap paramBitmap, SparseArray<Face> paramSparseArray)
    {
        this.mBitmap = paramBitmap;
        this.mFaces = paramSparseArray;
        invalidate();
    }
}


/* Location:              R:\dex2jar-2.0\classes-dex2jar.jar!\com\google\android\gms\samples\vision\face\photo\FaceView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */