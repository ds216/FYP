package com.google.android.gms.samples.vision.face.photo;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.samples.vision.face.patch.SafeFaceDetector;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;

import java.io.InputStream;

public class PhotoViewerActivity extends Activity
{
    private static final String TAG = "PhotoViewerActivity";

    double dist(PointF pt1, PointF pt2)
    {
        return Math.sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
    }

    PointF getNoseBase(Face face)
    {
        PointF pos = new PointF();
        for(Landmark lm : face.getLandmarks()){
            if (lm.getType() == Landmark.NOSE_BASE)
            {
                pos.x = lm.getPosition().x;
                pos.y = lm.getPosition().y;
            }
        }
        return pos;
    }

    float getrdist(Face face)
    {
        double d = 0;
        int i = 0;
        for(Landmark lm : face.getLandmarks()){
            if (lm.getType() == Landmark.LEFT_CHEEK || lm.getType()==Landmark.RIGHT_CHEEK)
            {
                d+=dist(lm.getPosition(),getNoseBase(face));
                i++;
            }
        }
        return (float)(d / i);
    }

    String landmark(int paramInt)
    {
        return new String[] { "BOTTOM_MOUTH", "LEFT_CHEEK", "LEFT_EAR_TIP", "LEFT_EAR", "LEFT_EYE", "LEFT_MOUTH", "NOSE_BASE", "RIGHT_CHEEK", "RIGHT_EAR_TIP", "RIGHT_EAR", "RIGHT_EYE", "RIGHT_MOUTH" }[paramInt];
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_photo_viewer);
        InputStream stream = getResources().openRawResource(R.raw.face13);
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        FaceDetector detector = new FaceDetector.Builder(getApplicationContext())
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .build();
        Detector<Face> safeDetector = new SafeFaceDetector(detector);
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
        SparseArray<Face> faces = safeDetector.detect(frame);
        for(int i=0; i< faces.size();i++){
            int j = 1;
            int faceNumber = 13;
            if(faceNumber==0) faceNumber = i;
            Face face = faces.get(i);
            Log.d("Face #" + i, "degree = " + face.getEulerY() + "," + face.getEulerZ());
            PointF midPt = new PointF();
            midPt.x = (face.getPosition().x + face.getWidth() / 2.0F);
            midPt.y = (face.getPosition().y + face.getHeight() / 2.0F);

            for(Landmark lm : face.getLandmarks()){
                PointF noseBase = getNoseBase(face);
                PointF rp = rotate_point(getNoseBase(face), face.getEulerZ(), lm.getPosition());
                float f = getrdist(face);
                Log.d("Face #" + faceNumber, "Landmark #" + j + " [" + landmark(lm.getType()) + "]\t: Rx = " + (noseBase.x - rp.x) / f + ",\tRy = " + (noseBase.y - rp.y) / f);
                j += 1;
            }
        }
        if (!safeDetector.isOperational())
        {
            Log.w(TAG, "Face detector dependencies are not yet available.");
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }
        FaceView overlay = (FaceView) findViewById(R.id.faceView);
        overlay.setContent(bitmap, faces);

        // Although detector may be used multiple times for different images, it should be released
        // when it is no longer needed in order to free native resources.
        safeDetector.release();
    }

    PointF rotate_point(PointF c, float degree, PointF p)
    {
        double d = Math.toRadians(degree);
        float sin = (float)Math.sin(d);
        float cos = (float)Math.cos(d);
        p.x -= c.x;
        p.y -= c.y;
        // rotate point
        float xnew = p.x * cos - p.y * sin;
        float ynew = p.x * sin + p.y * cos;

        // translate point back:
        p.x = xnew + c.x;
        p.y = ynew + c.y;
        return p;
    }
}


/* Location:              R:\dex2jar-2.0\classes-dex2jar.jar!\com\google\android\gms\samples\vision\face\photo\PhotoViewerActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */