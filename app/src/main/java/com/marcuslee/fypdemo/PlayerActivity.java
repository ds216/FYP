package com.marcuslee.fypdemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.media.FaceDetector;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Landmark;

import com.marcuslee.fypdemo.db.FacesDAO;
import com.marcuslee.fypdemo.db.GroupingDAO;
import com.marcuslee.fypdemo.db.MoviesDAO;
import com.marcuslee.fypdemo.db.PersonsDAO;
import com.marcuslee.fypdemo.db.obj.DBItem;
import com.marcuslee.fypdemo.db.obj.Faces;
import com.marcuslee.fypdemo.db.obj.Grouping;
import com.marcuslee.fypdemo.db.obj.Movies;
import com.marcuslee.fypdemo.db.obj.Persons;
import com.marcuslee.fypdemo.obj.Face;
import com.marcuslee.fypdemo.obj.Keyframe;
import com.marcuslee.fypdemo.obj.Movie;
import com.marcuslee.fypdemo.obj.Person;
import com.marcuslee.fypdemo.obj.Shot;
import com.marcuslee.fypdemo.util.FaceTracker;
import com.marcuslee.fypdemo.util.Logger;
import com.marcuslee.fypdemo.util.MetadataParser;
import com.marcuslee.fypdemo.util.ObjectHolder;
import com.marcuslee.fypdemo.util.SafeFaceDetector;
import com.marcuslee.fypdemo.view.FaceDetectView;
import com.marcuslee.fypdemo.view.FaceDrawer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;


public class PlayerActivity extends AppCompatActivity {
	private FaceDetectView vv = null;
	private FaceDrawer fd;
	private View decorView;
	private Handler h;
	private int delay = 150; //milliseconds
	private Boolean active = true;
	private int navsize = 0, playedms;
	private MediaPlayer mep;
	private MediaMetadataRetriever retriever;
	private static String testingFile = "v3"; // get Extrs
	private Boolean hasMetadata = !true;//get Extra

	private FacesDAO facesDAO;
	private PersonsDAO personsDAO;
	private MoviesDAO moviesDAO;
	private GroupingDAO groupingDAO;

	//attr for update the info window
	private static RelativeLayout info;
	private static TextView tvPersonName;
	private static TextView tvPersonDescription;
	private static Boolean faceSelected = false;
	private static Button WebButton, YoutubeButton;
	private static Face Selected;
	private static final int ThreadNumber = 8;
	private static int FinishThread = 0;
	private static int Processed = 0;
	private ProgressDialog mDialog;

	static int AAcount = 0, AAscount = 0;

	static String videoPath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		FullScreenActivity();
		super.onCreate(savedInstanceState);

		Logger.debug("LifeCycle", "onCreate");

		// Testing Start
		facesDAO = new FacesDAO(getApplicationContext());
		personsDAO = new PersonsDAO(getApplicationContext());
		moviesDAO = new MoviesDAO(getApplicationContext());
		groupingDAO = new GroupingDAO(getApplicationContext());

		Logger.enable();
		List<DBItem> items = facesDAO.getAll();
		Logger.debug("facesDAO test", "N = " + items.size());
		for (DBItem item : items) {
			Faces faces = (Faces) item;
			Logger.debug("facesDAO test", faces.toString());
		}

		List<DBItem> items2 = personsDAO.getAll();
		Logger.debug("personsDAO test", "N = " + items2.size());
		for (DBItem item : items2) {
			Persons faces = (Persons) item;
			Logger.debug("personsDAO test", faces.toString());
		}

		List<DBItem> items3 = moviesDAO.getAll();
		Logger.debug("moviesDAO test", "N = " + items3.size());
		for (DBItem item : items3) {
			Movies faces = (Movies) item;
			Logger.debug("moviesDAO test", faces.toString());
		}

		List<DBItem> items4 = groupingDAO.getAll();
		Logger.debug("groupingDAO test", "N = " + items4.size());
		for (DBItem item : items4) {
			Grouping faces = (Grouping) item;
			Logger.debug("groupingDAO test", faces.toString());
		}
		moviesDAO.close();
		facesDAO.close();
		personsDAO.close();
		groupingDAO.close();


		String[] projection = new String[]{
				MediaStore.MediaColumns.DISPLAY_NAME
				, MediaStore.MediaColumns.DATE_ADDED
				, MediaStore.MediaColumns.MIME_TYPE
				, MediaStore.MediaColumns.DATA
		};
		Cursor mCur = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				projection, null, null, null
		);
		mCur.moveToFirst();

		while (mCur.isAfterLast() == false) {
			String s = "";
			for (int i = 0; i < mCur.getColumnCount(); i++) {
				if (!s.isEmpty()) s += ", ";
				s += mCur.getColumnName(i) + " = " + mCur.getString(i);
			}
			Logger.debug("MediaStore", s);
			mCur.moveToNext();
		}

		ArrayList<Movie> lists = getMovieList();
		for (Movie movi : lists) {
			Logger.debug("MovieList", movi.toString());
		}
		Logger.disable();

		// Testing End

		setContentView(R.layout.activity_main);
		Intent it = this.getIntent();
		videoPath = it.getStringExtra("videoPath");
		hasMetadata = it.getBooleanExtra("hasMetaData",false);
		Logger.enable();
		Logger.debug("Extra","videoPath = "+videoPath+", "+hasMetadata);
		Logger.disable();

		h = new Handler();
		retriever = new MediaMetadataRetriever();
		decorView = getWindow().getDecorView();

		int resId = getResources().getIdentifier("raw/" + testingFile, null, this.getPackageName());
		String path;
		if (videoPath == null || (videoPath != null && videoPath.isEmpty())) {
			path = "android.resource://" + getPackageName() + "/" + resId;
		} else {
			path = videoPath;
		}
		//hasMetadata = HasMetadata(Uri.parse(path));
		Logger.enable();
		Logger.debug("hasMetadata"," = " + hasMetadata);
		Logger.disable();
		videoPath = path;
		final Uri uri = Uri.parse(path);
		retriever.setDataSource(getApplication(), uri);
		info = (RelativeLayout) findViewById(R.id.info);
		fd = new FaceDrawer(this);
		vv = (FaceDetectView) findViewById(R.id.videoView);
		vv.setWillNotDraw(false);
		Resources resources = getResources();
		int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
		if (resourceId > 0) {
			navsize = resources.getDimensionPixelSize(resourceId);
		}
		vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) { //code to run after video was prepared
				mep = mp;
				Display display = getWindowManager().getDefaultDisplay();
				Point size = new Point();
				String tag = "onPrepared";
				display.getSize(size);
				int width = size.x;
				int height = size.y;
				FaceDrawer.setScreenHeight(height);
				FaceDrawer.setScreenWidth(width + navsize);
				FaceDrawer.setVideoHeight(mp.getVideoHeight());
				FaceDrawer.setVideoWidth(mp.getVideoWidth());
				Logger.debug(tag, "Srceen size = W = " + width + ", H = " + height + "Nav Size = " + navsize); // Srceen W = 1794, H = 1080
				Logger.debug(tag, "videoview size = W = " + vv.getWidth() + ", H = " + vv.getHeight()); //videoview W = 1794, H = 1080
				Logger.debug(tag, "Video W = " + mp.getVideoWidth() + ", H = " + mp.getVideoHeight()); // Video W = 320, H = 240
				fd.init();
				if (playedms != 0) mep.seekTo(playedms);
			}
		});
		vv.setMediaController(new FullMediaController(this));
		// set video
		vv.setVideoPath(path);
		HideInfo();
		if(hasMetadata){
			String npath = uri.getPath();
			int ind = npath.lastIndexOf('/');
			npath = npath.substring(0, ind);
			int index = uri.getLastPathSegment().lastIndexOf('.');
			npath += "/" + uri.getLastPathSegment().substring(0, index) + ".dat";
			File f = new File(npath);
			parseDat(f);
			vv.start();
		}
		LinearLayout ll = (LinearLayout) findViewById(R.id.overlay);
		ll.addView(fd);

		tvPersonName = (TextView) findViewById(R.id.PersonName);
		tvPersonDescription = (TextView) findViewById(R.id.PersonDescription);
		WebButton = (Button) findViewById(R.id.WebButton);
		YoutubeButton = (Button) findViewById(R.id.YoutubeButton);

		WebButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				OpenURL(0);
			}
		});
		YoutubeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				OpenURL(1);
			}
		});

		Button close = (Button) findViewById(R.id.closeInfo);
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				HideInfo();
			}
		});

		findViewById(android.R.id.content).setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					int frameNo = (int) Math.floor(mep.getCurrentPosition() * 29.97 / 1000); // FPS = frame / s  =>  frame = FPS * s  =>  frame = FPS * ms /1000
					Logger.debug("FaceTouch", "Frame = " + frameNo + ", Pos = " + mep.getCurrentPosition());
					ObjectHolder OH = ObjectHolder.getInstance();
					Shot shot = OH.findShot(frameNo);
					if (shot!=null && shot.hasKeyframe()) {
						Keyframe keyframe = shot.getKeyframe();
						if (keyframe.countfaces() > 0) {
							float touchX, touchY;
							touchX = event.getX();
							touchY = event.getY();
							for (Face f : keyframe.getFaces()) {
								Rect r = FaceDrawer.CalculateRect(f);
								if (r.contains((int) touchX, (int) touchY)) {
									Logger.debug("FaceTouch", "You have touch the face : " + f.getPerson().getName());
									//new GetWebContent().execute(f.getPerson().getName());
									setFace(f);
									ShowInfo();
								}
							}
						}
					}
				}
				return true;
			}
		});

		if (active) {
			h.postDelayed(new Runnable() {
				public void run() {//this method will run every 100ms. and it will invalidate the FaceDrawer to call the onDraw() again.
					try {
						if (mep != null && mep.isPlaying()) {
							playedms = mep.getCurrentPosition();
							int frameNo = (int) Math.floor(mep.getCurrentPosition() * 29.97 / 1000); // FPS = frame / s  =>  frame = FPS * s  =>  frame = FPS * ms /1000
							int faceLimit = 0;
							Logger.debug("postDelayed", "Frame No. = " + frameNo);
							Logger.debug("Handler", "Second = " + mep.getCurrentPosition() + " = " + mep.getCurrentPosition() / 1000.00 + ", frame = " + frameNo);
							FaceDrawer.setCurrentSecond(mep.getCurrentPosition());

							ObjectHolder OH = ObjectHolder.getInstance();
							Shot shot = OH.findShot(frameNo);
							if (shot != null && shot.hasKeyframe()) {
								Keyframe keyframe = shot.getKeyframe();
								if (keyframe.countfaces() > 0) {
									FaceDetectView.setHasFace(true);
									FaceDetectView.setFaces(keyframe.getFaces());
									faceLimit = keyframe.countfaces();
								} else {
									FaceDetectView.setHasFace(false);
								}
							}
							Point size = new Point();
							Display display = getWindowManager().getDefaultDisplay();
							display.getSize(size);

							View v1 = getWindow().getDecorView().getRootView();
							v1.setDrawingCacheEnabled(true);
							Bitmap bitmap = retriever.getFrameAtTime(mep.getCurrentPosition() * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
							Logger.debug("Bitmap", "W = " + bitmap.getWidth() + ", H = " + bitmap.getHeight());
							android.media.FaceDetector.Face[] faces = new FaceDetector.Face[faceLimit];
							FaceDetector detector = new FaceDetector(bitmap.getWidth(), bitmap.getHeight(), faceLimit);
							int facecount = detector.findFaces(bitmap, faces);
							Logger.debug("Face_Detection", "Face Count: " + String.valueOf(facecount));
							FaceTracker ft = FaceTracker.getInstance();
							if (shot.getShotId() != ft.getShotid()) {
								ft.clear();//reset the tracker when a new shot start
								ft.setShotid(shot.getShotId());
								if (shot.hasKeyframe()) ft.setKeyFrame(shot.getKeyframe());
							}
							if (facecount > 0) {
								ft.track(faces);
								FaceDrawer.setDetectedFace(faces);
							} else {
								FaceDrawer.setDetectedFace(null);
							}
							fd.invalidate();
						}
					} catch (IllegalStateException e) {

					}
					h.postDelayed(this, delay);
				}
			}, delay);
		}

		if (hasMetadata) {
			RecordFacePattern task = new RecordFacePattern();
			task.execute();
		}else{
			FinishThread = 0;
			Processed = 0;
				FaceRecognizer[] tasks = new FaceRecognizer[ThreadNumber];
				for (int i = 0; i < ThreadNumber; i++) {
					tasks[i] = new FaceRecognizer();
					tasks[i].executeOnExecutor(Executors.newCachedThreadPool(), new Integer[]{i * 20, 20 * ThreadNumber});
				}
				tasks = null;
		}
	}

	public void parseDat() {
		int resId = getResources().getIdentifier("raw/" + testingFile + "_face", null, this.getPackageName());
		if(resId > 0) {
			try {
				InputStream is = getResources().openRawResource(resId);
				parseDat(is);
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void parseDat(File f) {
		InputStream is = null;
		try {
			is = new FileInputStream(f);
			parseDat(is);
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void parseDat(InputStream is){
		MetadataParser mp = new MetadataParser();
		mp.ParseData(is);
	}

	@Override
	public void onPause() {
		super.onPause();
		Logger.debug("LifeCycle", "onPause");
	}

	public void onStop() {
		super.onStop();
		Logger.debug("LifeCycle", "onStop");
	}

	public void onRestart() {
		super.onRestart();
		Logger.debug("LifeCycle", "onRestart");
	}

	public void onStart() {
		super.onStart();
		Logger.debug("LifeCycle", "onStart");
	}

	public void onResume() {
		super.onResume();
		Logger.debug("LifeCycle", "onResume");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("saved_state", playedms + "");
		/*Toast.makeText(PlayerActivity.this,
				"onSaveInstanceState:\n" +
						"saved_state = " + playedms,
				Toast.LENGTH_LONG).show();*/
		Logger.debug("LifeCycle", "onSaveInstanceState");
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		String stateSaved = savedInstanceState.getString("saved_state");
		if (stateSaved == null) {
			Toast.makeText(PlayerActivity.this,
					"onRestoreInstanceState:\n" +
							"NO state saved!",
					Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(PlayerActivity.this,
					"onRestoreInstanceState:\n" +
							"saved state = " + stateSaved,
					Toast.LENGTH_LONG).show();
			playedms = Integer.parseInt(stateSaved);
		}

	}

	public static void HideInfo() {
		if (info != null)
			info.setVisibility(View.GONE);
		faceSelected = false;
		Selected = null;
	}

	public static void ShowInfo() {
		if (info != null)
			info.setVisibility(View.VISIBLE);
		faceSelected = true;
	}

	public static void setFace(final Face face) {
		faceSelected = true;
		tvPersonName.setText(face.getPerson().getName());
		String str = face.getPerson().getDescription();
		if (str == null || str.isEmpty())
			str = "No description now. :(";
		tvPersonDescription.setText(str);
		Selected = face;
	}

	public static Boolean isFaceSelected() {
		return faceSelected;
	}

	public void OpenURL(int flag) {
		playedms = mep.getCurrentPosition();
		String str;
		if (Selected != null) {
			if (flag == 0) {
				str = "https://www.google.com.hk/#q=" + Selected.getPerson().getName();
			} else {
				str = "https://www.youtube.com/results?search_query=" + Selected.getPerson().getName();
			}
			OpenURL(str);
		}
	}

	public void OpenURL(String url) {
		Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}

	public ArrayList<Movie> getMovieList() {
		ArrayList<Movie> list = new ArrayList<>();
		String[] projection = new String[]{
				MediaStore.MediaColumns.DISPLAY_NAME
				, MediaStore.MediaColumns.DATE_ADDED
				, MediaStore.MediaColumns.MIME_TYPE
				, MediaStore.MediaColumns.DATA
		};
		Cursor mCur = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				projection, null, null, null
		);
		mCur.moveToFirst();

		while (mCur.isAfterLast() == false) {
			Movie movie = new Movie();
			Uri uri = Uri.parse(mCur.getString(3));
			movie.setPath(uri);
			String npath = uri.getPath();
			int ind = npath.lastIndexOf('/');
			npath = npath.substring(0, ind);
			int index = mCur.getString(0).lastIndexOf('.');
			npath += "/" + mCur.getString(0).substring(0, index) + ".dat";
			Logger.debug("getMovieList", npath);
			File f = new File(npath);
			if (f.exists()) {
				movie.setHasMetadata(true);
				Logger.debug("getMovieList", "Metadata presented.");
			} else {
				Logger.debug("getMovieList", "No metadata presented.");
			}
			list.add(movie);
			mCur.moveToNext();
		}
		return list;
	}
	public Boolean HasMetadata(Uri path){
		String spath = path.getPath();
		String npath;
		int ind = spath.lastIndexOf('/');
		npath = spath.substring(0, ind);
		int index = spath.lastIndexOf('.');
		npath += "/" + spath.substring(0, index) + ".dat";
		Logger.debug("HasMetadata", npath);
		File f = new File(npath);
		if (f.exists()) {
			return true;
		} else {
			return false;
		}
	}

	private void FullScreenActivity() {
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav                                                               // bar
						| View.SYSTEM_UI_FLAG_FULLSCREEN// hide status bar;
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	public class FaceRecognizer extends AsyncTask<Integer, Integer, Void>{

		@Override
		protected void onPreExecute() {
			if(mDialog == null) {
				mDialog = new ProgressDialog(PlayerActivity.this);
				mDialog.setMessage("Loading...");
				mDialog.setCancelable(false);
				mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
				int t = Integer.parseInt(time);
				t =(int)Math.round(Math.ceil(t / 1000 * 29.97 / 20));
				mDialog.setMax(t);
				mDialog.show();
				getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
				FullScreenActivity();
			}
		}

		@Override
		protected Void doInBackground(Integer... params) {
			//final int fs = 60;
			int fs = params[1];
			int ps = params[0];
			Logger.enable();
			Logger.debug("AsyncTask", "start("+ps+", "+fs+") " + System.currentTimeMillis());
			ObjectHolder oj = ObjectHolder.getInstance();

			facesDAO = new FacesDAO(getApplicationContext());
			personsDAO = new PersonsDAO(getApplicationContext());
			moviesDAO = new MoviesDAO(getApplicationContext());
			groupingDAO = new GroupingDAO(getApplicationContext());
			ArrayList<DBItem> facesList =  (ArrayList<DBItem>) facesDAO.getAll();
			//mep.setDataSource(videoPath);
			String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			Double du = (Long.parseLong(time)/1000 * 29.97); // total frame number
			int frameNo = du.intValue();
			int curFrame = 0, shotId = 1;
			for(int start=ps;start<frameNo;start+=fs){ //each 10 frame as a shot
				int end = start+fs;
				curFrame = (start+end)/2;
				double second = curFrame/29.97;
				//Logger.debug("FaceRecognizer","Frame #" + curFrame +" / "+frameNo+","+second+"sec ");
				Bitmap bitmap = retriever.getFrameAtTime((long)Math.floor(second*1000*1000), MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
				if(bitmap!=null){ // frame is gettable
					com.google.android.gms.vision.face.FaceDetector fdetector = new com.google.android.gms.vision.face.FaceDetector.Builder(getApplicationContext())
							.setTrackingEnabled(false)
							.setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
							.build();
					Detector<com.google.android.gms.vision.face.Face> fsafeDetector = new SafeFaceDetector(fdetector);
					Frame frame = new Frame.Builder().setBitmap(bitmap).build();
					SparseArray<com.google.android.gms.vision.face.Face> faces = fsafeDetector.detect(frame);
					//Logger.debug("FaceRecognizer","faces count = "+faces.size());
					Keyframe newKeyframe = new Keyframe(curFrame);
					for(int i=0;i<faces.size();i++){ // for each detected face
						int res;
						int key = faces.keyAt(i);
						//Logger.debug("Loop","GETTING DETECTED FACE #"+i+", is Face null? = "+(faces.get(key)==null));
						if(faces.get(key)==null) continue;
						HashMap<Faces,Float[]> result = new HashMap<>();
						for(int j = 0;j<facesList.size();j++){ // compare with the store face
							Float[] ress = new Float[5];
							com.marcuslee.fypdemo.util.FaceRecognizer fr = new com.marcuslee.fypdemo.util.FaceRecognizer();
							res = fr.cmp(faces.get(key),(Faces)facesList.get(j));
							ress[0] = (float) res; //number of match
							ress[1] = faces.get(key).getPosition().x; //x-pos
							ress[2] = faces.get(key).getPosition().y; //y-pos
							ress[3] = faces.get(key).getWidth(); //Width
							ress[4] = faces.get(key).getHeight(); //Height
							result.put((Faces)facesList.get(j),ress);
							AAcount++;

						}
						Iterator it = result.entrySet().iterator();
						int max=4,ho=0;
						String log = "";
						while (it.hasNext()) {
							Map.Entry pair = (Map.Entry)it.next();
							Faces f = (Faces)pair.getKey();
							Float[] farr = (Float[]) pair.getValue();
							if(!log.isEmpty()) log += ", ";
							log += f.getPerson().getName() + " = " + farr[0];
							if(f.getPerson().getName().equals("Ho Chun-yan")){
								ho = Math.round(farr[0]);
							}
							if(max< farr[0]){
								max = Math.round(farr[0]);
							}
						}
						Logger.debug("FaceRecognizer", "Frame #" + curFrame + " : " + log);
						if(ho == max)AAscount++;
						Iterator it2 = result.entrySet().iterator();

						while (it2.hasNext()) {
							Map.Entry pair = (Map.Entry)it2.next();
							Faces fas = (Faces)pair.getKey();
							Float[] farr = (Float[]) pair.getValue();
							if(Math.floor(farr[0])==max){
								Face f = new Face();
								f.setPerson(fas.getPerson());
								f.setHeight(Math.round(farr[1]));
								f.setWidth(Math.round(farr[2]));
								f.setPosX(Math.round(farr[3]));
								f.setPosY(Math.round(farr[4]));
								newKeyframe.addFace(f);
								break;
							}
						}

					}
					Shot newShot = new Shot(newKeyframe,shotId,start,end);
					oj.addShot(newShot);
					faces = null;
					fsafeDetector.release();

				}
				Processed++;
				publishProgress(Processed);
				if(end > frameNo)break;//end of video
			}

			Logger.debug("FaceRecognizer", "Count = "+AAcount+", S = "+AAscount);
			Logger.debug("AsyncTask","End "+System.currentTimeMillis());
			Logger.debug("Result",com.marcuslee.fypdemo.util.FaceRecognizer.print());
			Logger.disable();
			facesDAO.close();
			personsDAO.close();
			moviesDAO.close();
			return null;
		}

		@Override
		protected void onPostExecute(Void result){
			FinishThread++;
			if(FinishThread==ThreadNumber) {
				vv.start(); //start playing video after finish
				mDialog.dismiss();
			}
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
			FullScreenActivity();
		}

		@Override
		protected void onProgressUpdate (Integer... progress) {
			mDialog.setProgress(progress[0]);
		}
	}

	public class RecordFacePattern extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			Logger.enable();
			Logger.debug("AsyncTask", "start");
			facesDAO = new FacesDAO(getApplicationContext());
			personsDAO = new PersonsDAO(getApplicationContext());
			moviesDAO = new MoviesDAO(getApplicationContext());
			groupingDAO = new GroupingDAO(getApplicationContext());
			if (moviesDAO.getByFilename(videoPath) == null) {
				Movies movies = new Movies();
				movies.setFileName(videoPath);
				moviesDAO.insert(movies);
				movies = (Movies)moviesDAO.getByFilename(videoPath);
				ObjectHolder oh = ObjectHolder.getInstance();
				ArrayList<Shot> shots = oh.getShots();
				com.google.android.gms.vision.face.FaceDetector detector = new com.google.android.gms.vision.face.FaceDetector.Builder(getApplicationContext())
						.setTrackingEnabled(false)
						.setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
						.build();
				Detector<com.google.android.gms.vision.face.Face> safeDetector = new SafeFaceDetector(detector);
				int faceNumber = 0;
				for (Shot s : shots) {
					if (s.hasKeyframe()) {
						int second = (int) (s.getKeyframe().getFramenumber() / 29.97);
						Bitmap bitmap = retriever.getFrameAtTime(second * 1000 * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
						Logger.debug("AsyncTask", "Getting Frame from " + second + " us");

						Frame frame = new Frame.Builder().setBitmap(bitmap).build();
						SparseArray<com.google.android.gms.vision.face.Face> faces = safeDetector.detect(frame);

						ArrayList<Face> faceData = s.getKeyframe().getFaces();
						Logger.debug("AsyncTask", "No. of Faces = " + faces.size());
						HashMap<Face, com.google.android.gms.vision.face.Face> FaceMap = new HashMap<>();
						for (int i = 0; i < faces.size(); i++) {
							int j = 1;
							double minDist = Double.POSITIVE_INFINITY;
							Face possibleFace = null;
							int key = faces.keyAt(i);
							com.google.android.gms.vision.face.Face face = faces.get(key);
							if (face != null && face.getLandmarks().size()==8) {
								Logger.debug("Face #" + faceNumber, "degree = " + face.getEulerY() + "," + face.getEulerZ());
								PointF midPt = new PointF();
								midPt.x = (face.getPosition().x + face.getWidth() / 2.0F);
								midPt.y = (face.getPosition().y + face.getHeight() / 2.0F);

								for (Face f : faceData) {
									if (!FaceMap.containsKey(f)) {
										double dist = dist(midPt, f.getMidPoint());
										if (dist < minDist) {
											minDist = dist;
											possibleFace = f;
										}
										Logger.debug("AsyncTask", "Comparing Face #" + faceNumber + " with " + f.getPerson().getName() + " : " + dist);
									}
								}
								if (possibleFace != null) {
									FaceMap.put(possibleFace, face);

								}
								/*
								for (Landmark lm : face.getLandmarks()) {
									PointF noseBase = getNoseBase(face);
									PointF rp = rotate_point(getNoseBase(face), face.getEulerZ(), lm.getPosition());
									float f = getrdist(face);
									Logger.debug("Face #" + faceNumber, "Landmark #" + j + " [" + landmark(lm.getType()) + "]\t: Rx = " + (noseBase.x - rp.x) / f + ",\tRy = " + (noseBase.y - rp.y) / f);
									j += 1;
								}*/
								faceNumber++;
							}
						}
						Iterator iterator = FaceMap.keySet().iterator();

						while (iterator.hasNext()) {
							Face key = (Face) iterator.next();
							com.google.android.gms.vision.face.Face value = FaceMap.get(key);
							PointF noseBase = getNoseBase(value);
							Persons pitem = new Persons();
							Faces fitem = new Faces();
							pitem.setPerson(key.getPerson());
							fitem.setPerson(key.getPerson());
							List<Landmark> landmarks = value.getLandmarks();
							int j = 1;
							for (Landmark lm : landmarks) {
								PointF rp = rotate_point(getNoseBase(value), value.getEulerZ(), lm.getPosition());
								float f = getrdist(value);
								PointF result = new PointF();
								result.x = (noseBase.x - rp.x) / f;
								result.y = (noseBase.y - rp.y) / f;
								Logger.debug("Face #" + faceNumber, "Landmark #" + j + " [" + landmark(lm.getType()) + "]\t: Rx = " + result + ",\tRy = " + result);
								j += 1;
								switch (lm.getType()) {
									case 0:
										fitem.setBottom_mouth(result);
										break;
									case 1:
										fitem.setLeft_cheek(result);
										break;
									case 2:
										fitem.setLeft_ear_tip(result);
										break;
									case 3:
										fitem.setLeft_ear(result);
										break;
									case 4:
										fitem.setLeft_eye(result);
										break;
									case 5:
										fitem.setLeft_mouth(result);
										break;
									case 6:
										fitem.setNose_base(result);
										break;
									case 7:
										fitem.setRight_cheek(result);
										break;
									case 8:
										fitem.setRight_ear_tip(result);
										break;
									case 9:
										fitem.setRight_ear(result);
										break;
									case 10:
										fitem.setRight_eye(result);
										break;
									case 11:
										fitem.setRight_mouth(result);
										break;
								}
							}
							if (personsDAO.getByPersonName(pitem.getPerson().getName()) != null) {
								facesDAO.update(fitem);

							} else {
								facesDAO.insert(fitem);
								personsDAO.insert(pitem);
							}
							pitem = personsDAO.getByPersonName(pitem.getPerson().getName());
							Grouping gitem = new Grouping(pitem,movies);
							Logger.debug("gitem","pitem = "+pitem);
							Logger.debug("gitem", "movies = " + movies);
							if(groupingDAO.get(gitem)==null)
								groupingDAO.insert(gitem);
							Logger.debug("AsyncTask", key.getPerson().getName() + " : DataPos = (" + key.getMidPoint().x + "," + key.getMidPoint().y + ")" + ", detected = (" + (value.getPosition().x + value.getWidth() / 2) + "," + (value.getPosition().y + value.getHeight() / 2) + ")");
						}
						Logger.debug("AsyncTask", bitmap.toString());
					}
				}
				safeDetector.release();


			}
			facesDAO.close();
			personsDAO.close();
			moviesDAO.close();
			groupingDAO.close();
			Logger.debug("AsyncTask", "End");
			Logger.disable();
			return null;
		}

		double dist(PointF pt1, PointF pt2) {
			return Math.sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
		}

		PointF getNoseBase(com.google.android.gms.vision.face.Face face) {
			PointF pos = new PointF();
			for (Landmark lm : face.getLandmarks()) {
				if (lm.getType() == Landmark.NOSE_BASE) {
					pos.x = lm.getPosition().x;
					pos.y = lm.getPosition().y;
				}
			}
			return pos;
		}

		float getrdist(com.google.android.gms.vision.face.Face face) {
			double d = 0;
			int i = 0;
			for (Landmark lm : face.getLandmarks()) {
				if (lm.getType() == Landmark.LEFT_CHEEK || lm.getType() == Landmark.RIGHT_CHEEK) {
					d += dist(lm.getPosition(), getNoseBase(face));
					i++;
				}
			}
			return (float) (d / i);
		}

		PointF rotate_point(PointF c, float degree, PointF p) {
			/*
			double d = Math.toRadians(degree);
			float sin = (float) Math.sin(d);
			float cos = (float) Math.cos(d);
			p.x -= c.x;
			p.y -= c.y;
			// rotate point
			float xnew = p.x * cos - p.y * sin;
			float ynew = p.x * sin + p.y * cos;

			// translate point back:
			p.x = xnew + c.x;
			p.y = ynew + c.y;*/
			return p;
		}

		String landmark(int paramInt) {
			return new String[]{"BOTTOM_MOUTH", "LEFT_CHEEK", "LEFT_EAR_TIP", "LEFT_EAR", "LEFT_EYE", "LEFT_MOUTH", "NOSE_BASE", "RIGHT_CHEEK", "RIGHT_EAR_TIP", "RIGHT_EAR", "RIGHT_EYE", "RIGHT_MOUTH"}[paramInt];
		}


	}

	private class FullMediaController extends MediaController {
		public FullMediaController(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		public FullMediaController(Context context, boolean useFastForward) {
			super(context, useFastForward);
		}

		public FullMediaController(Context context) {
			super(context);
		}

		@Override
		public void show() {
			super.show();
			FullScreenActivity();
		}

		@Override
		public void hide() {
			super.hide();
			FullScreenActivity();
		}
	}
}
