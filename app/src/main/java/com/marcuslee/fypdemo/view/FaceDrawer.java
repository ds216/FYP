package com.marcuslee.fypdemo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.View;

import com.marcuslee.fypdemo.PlayerActivity;
import com.marcuslee.fypdemo.obj.Face;
import com.marcuslee.fypdemo.obj.Keyframe;
import com.marcuslee.fypdemo.obj.Shot;
import com.marcuslee.fypdemo.util.Logger;
import com.marcuslee.fypdemo.util.ObjectHolder;

import java.util.ArrayList;

/**
 * Created by Marcus on 9/29/2015.
 */
public class FaceDrawer extends View {
	private Paint paint;
	static double sx = 0;
	static double sy = 0;
	static double Multiplier = 0;
	static double ScreenHeight = 0, ScreenWidth = 0;
	static double VideoHeight = 0, VideoWidth = 0;
	static int currentSecond = 0;
	static android.media.FaceDetector.Face[] detectedFace;

	PointF myMidPoint = new PointF();


	public FaceDrawer(Context context) {
		super(context);
		paint = new Paint();
	}

	public void init() {
		if (ScreenHeight > 0 && ScreenWidth > 0 && VideoHeight > 0 && VideoWidth > 0) {
			if (ScreenHeight > VideoHeight) {
				double diff = ScreenHeight / VideoHeight;
				setMultiplier(diff);
				double aHeight = VideoHeight * Multiplier;
				Logger.debug("Act. Height = " + aHeight);
				double aWidth = VideoWidth * Multiplier;
				Logger.debug("Act. Width = " + aWidth);
				setSy((ScreenHeight - aHeight) / 2);
				setSx((ScreenWidth - aWidth) / 2);
			}
		}
	}

	protected void onDraw(Canvas canvas) {
		//canvas.drawColor(Color.GREEN);
		paint.setColor(Color.GREEN);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(2);
		int frameNumber = (int) Math.floor(currentSecond * 29.97 / 1000);
		ObjectHolder OH = ObjectHolder.getInstance();
		Shot shot = OH.findShot(frameNumber);
		if (shot != null) {
			Keyframe Kf = shot.getKeyframe();
			if (Kf != null) {
				ArrayList<Face> faces = Kf.getFaces();
				if (faces != null && Kf.countfaces() > 0 && detectedFace == null) {
					for (Face f : faces) {
						Rect r = CalculateRect(f);
						canvas.drawRect(r, paint);
						Logger.debug("Shot = " + shot.getShotId() + "(" + shot.getStartFrame() + " - " + shot.getEndFrame() + ")");
						if (!PlayerActivity.isFaceSelected()) PlayerActivity.setFace(f);
					}
				}
			}
		}

		if (detectedFace != null) {
			paint.setColor(Color.RED);
			paint.setStyle(Paint.Style.STROKE);
			for (android.media.FaceDetector.Face f : detectedFace) {
				if (f != null) {
					Logger.debug("Face confidence" + f.confidence());

					f.getMidPoint(myMidPoint);
					float myEyesDistance = f.eyesDistance();
					Logger.debug("X = " + myMidPoint.x + ", Y = " + myMidPoint.y);
					Face fa = new Face();
					fa.setPosX((int) (myMidPoint.x - myEyesDistance));
					fa.setPosY((int) (myMidPoint.y - myEyesDistance));
					fa.setWidth((int) (myEyesDistance * 2));
					fa.setHeight((int) (myEyesDistance * 2));
					canvas.drawRect(CalculateRect(fa), paint);
					canvas.drawCircle((float) (sx + myMidPoint.x * Multiplier), (float) (sy + myMidPoint.y * Multiplier), 10, paint);
				}

			}
		}
	}

	public static Rect CalculateRect(Face f) {
		Rect rect = new Rect();
		if (Multiplier != 0) {
			float topx = f.getPosX();
			float topy = f.getPosY();
			float w = f.getWidth();
			float h = f.getHeight();
			float left = (float) (sx + topx * Multiplier);
			float top = (float) (sy + topy * Multiplier);
			float right = (float) (left + w * Multiplier);
			float bottom = (float) (top + h * Multiplier);
			rect.set((int) left, (int) top, (int) right, (int) bottom);
		} else
			rect.set(0, 0, 0, 0);
		return rect;
	}

	public static double getSx() {
		return sx;
	}

	public static void setSx(double sx) {
		FaceDrawer.sx = sx;
		Logger.debug("Sx set to " + FaceDrawer.sx);
	}

	public static double getSy() {
		return sy;
	}

	public static void setSy(double sy) {
		FaceDrawer.sy = sy;
		Logger.debug("Sy set to " + FaceDrawer.sy);
	}

	public static double getMultiplier() {
		return Multiplier;
	}

	public static void setMultiplier(double multiplier) {
		FaceDrawer.Multiplier = multiplier;
		Logger.debug("mutiplerx set to " + FaceDrawer.Multiplier);
	}

	public static double getScreenHeight() {
		return ScreenHeight;
	}

	public static void setScreenHeight(double screenHeight) {
		ScreenHeight = screenHeight;
		Logger.debug("ScreenHeight set to " + FaceDrawer.ScreenHeight);
	}

	public static double getScreenWidth() {
		return ScreenWidth;
	}

	public static void setScreenWidth(double screenWidth) {
		ScreenWidth = screenWidth;
		Logger.debug("ScreenWidth set to " + FaceDrawer.ScreenWidth);
	}

	public static double getVideoHeight() {
		return VideoHeight;
	}

	public static void setVideoHeight(double videoHeight) {
		VideoHeight = videoHeight;
		Logger.debug("VideoHeight set to " + FaceDrawer.VideoHeight);
	}

	public static double getVideoWidth() {
		return VideoWidth;
	}

	public static void setVideoWidth(double videoWidth) {
		VideoWidth = videoWidth;
		Logger.debug("VideoWidth set to " + FaceDrawer.VideoWidth);
	}

	public static int getCurrentSecond() {
		return currentSecond;
	}

	public static void setCurrentSecond(int currentSecond) {
		FaceDrawer.currentSecond = currentSecond;
	}

	public static android.media.FaceDetector.Face[] getDetectedFace() {
		return detectedFace;
	}

	public static void setDetectedFace(android.media.FaceDetector.Face[] detectedFace) {
		FaceDrawer.detectedFace = detectedFace;
	}
}
