package com.marcuslee.fypdemo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.VideoView;

import com.marcuslee.fypdemo.PlayerActivity;
import com.marcuslee.fypdemo.obj.Face;
import com.marcuslee.fypdemo.util.Logger;

import java.util.ArrayList;

/**
 * Created by Marcus on 10/4/2015.
 */
public class FaceDetectView extends VideoView {
	int frames = 0;
	private static boolean hasface = false;
	private static ArrayList<Face> faces;

	public FaceDetectView(Context context) {
		super(context);
	}

	public FaceDetectView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FaceDetectView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public static void setFaces(ArrayList<Face> faces) {
		FaceDetectView.faces = faces;
	}

	public static ArrayList<Face> getFaces() {
		return faces;
	}

	@Override
	public void start() {
		super.start();
		Logger.debug("Play", "Y");
		PlayerActivity.HideInfo();
	}

	@Override
	public void pause() {
		super.pause();
		Logger.debug("Pause", "Y");
		if (hasface) {
			int maxsize = 0;
			Face maxFace = new Face();
			for (Face f : faces) {
				int size = f.getHeight() * f.getWidth();
				if (size > maxsize) {
					maxsize = size;
					maxFace = f;
				}
			}
			PlayerActivity.setFace(maxFace);
			PlayerActivity.ShowInfo();
		}
	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

	public static void setHasFace(boolean b) {
		hasface = b;
	}
}
