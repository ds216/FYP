package com.marcuslee.fypdemo.util;

import com.marcuslee.fypdemo.obj.Face;
import com.marcuslee.fypdemo.obj.Keyframe;
import com.marcuslee.fypdemo.obj.Person;
import com.marcuslee.fypdemo.obj.Shot;

import java.util.ArrayList;

/**
 * Created by Marcus on 10/20/2015.
 */
public class ObjectHolder {
	private static ObjectHolder instance;
	private static ArrayList<Shot> Shots;
	private static ArrayList<Person> People;

	private ObjectHolder() {
		Shots = new ArrayList<>();
		People = new ArrayList<>();
	}

	public static ObjectHolder getInstance() {
		if (instance == null) instance = new ObjectHolder();
		return instance;
	}

	public static void addShot(Shot shot) {
		Shots.add(shot);
	}

	public static void addPerson(Person person) {
		People.add(person);
	}

	public Person getPerson(int id) {
		for (Person per : People) {
			if (per.getPersonid() == id) {
				return per;
			}
		}
		return null;//TODO:Exception
	}

	public Shot findShot(int frame) {
		for (Shot shot : Shots) {
			if (shot.getStartFrame() <= frame && shot.getEndFrame() > frame) {
				return shot;
			}
		}
		return null;//TODO:Exception
	}

	public Shot getShot(int id) {
		for (Shot shot : Shots) {
			if (shot.getShotId() == id) {
				return shot;
			}
		}
		return null;//TODO:Exception
	}

	public ArrayList<Shot> getShots() {
		return Shots;
	}

	public Boolean hasFace(int frameNumber) {
		ObjectHolder OH = ObjectHolder.getInstance();
		Shot shot = OH.findShot(frameNumber);
		if (shot != null) {
			Keyframe Kf = shot.getKeyframe();
			if (Kf != null) {
				ArrayList<Face> faces = Kf.getFaces();
				return faces != null && faces.size() > 0;
			}
		}
		return false;
	}
}
