package com.marcuslee.fypdemo.util;

import android.graphics.PointF;

import com.google.android.gms.vision.face.Landmark;
import com.marcuslee.fypdemo.db.obj.Faces;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 3/21/2016.
 */
public class FaceRecognizer {
	private static double R = 0.18;//0.10532
	static Double Maxmatch=Double.MIN_VALUE, MinmatchFloat=Double.MAX_VALUE, MAVG = 0.0d;
	static int Mcount=0, Tcount=0;
	static Double NMaxmatch=Double.MIN_VALUE, NMinmatchFloat=Double.MAX_VALUE, NAVG = 0.0d;
	static int NMcount=0, NTcount=0;

	public FaceRecognizer(){

	}

	public int cmp(com.google.android.gms.vision.face.Face df, Faces sf){
		int c = 0;
		//Logger.enable();
		// parsing detected face data

		PointF midPt = new PointF();
		midPt.x = (df.getPosition().x + df.getWidth() / 2.0F);
		midPt.y = (df.getPosition().y + df.getHeight() / 2.0F);
		//Logger.debug("cmp","midpt = "+midPt.x+","+midPt.y);
		// compare
		List<Landmark> lms = df.getLandmarks();
		for(Landmark l : lms){
			float x = l.getPosition().x;
			float y = l.getPosition().y;
			if(x == y && x == 0) break;
			int type = l.getType();
			PointF cp = new PointF();

			//parsing
			PointF rp = rotate_point(getNoseBase(df), df.getEulerZ(), l.getPosition());
			float f = getrdist(df);
			PointF result = new PointF();
			PointF noseBase = getNoseBase(df);
			result.x = (noseBase.x - rp.x) / f;
			result.y = (noseBase.y - rp.y) / f;
			//end

			switch(type){
				case 0:
					cp = sf.getBottom_mouth();
					break;
				case 1:
					cp = sf.getLeft_cheek();
					break;
				case 2:
					cp = sf.getLeft_ear_tip();
					break;
				case 3:
					cp = sf.getLeft_ear();
					break;
				case 4:
					cp = sf.getLeft_eye();
					break;
				case 5:
					cp = sf.getLeft_mouth();
					break;
				case 6:
					cp = sf.getNose_base();
					break;
				case 7:
					cp = sf.getRight_cheek();
					break;
				case 8:
					cp = sf.getRight_ear_tip();
					break;
				case 9:
					cp = sf.getRight_ear();
					break;
				case 10:
					cp = sf.getRight_eye();
					break;
				case 11:
					cp = sf.getRight_mouth();
					break;
			}
			if(!(cp.x==cp.y && cp.x==0)){
				double r = dif(result,cp);
				//Logger.enable();
				//Logger.debug("cmp","comparing to "+sf.getPerson().getName()+", R = "+r);
				if(sf.getPerson().getName().equals("Ho Chun-yan")){
					Tcount++;
					MAVG+=r;
					if(r>Maxmatch)Maxmatch=r;
					if(r<MinmatchFloat)MinmatchFloat=r;
					if( r <= R ){
						c++;
						Mcount++;
					}
				}else{
					NTcount++;
					NAVG+=r;
					if(r>NMaxmatch)NMaxmatch=r;
					if(r<NMinmatchFloat)NMinmatchFloat=r;
					if( r <= R ){
						c++;
						NMcount++;
					}
				}
				/*
				if( r <= R ){
					c++;
				}*/
			}
		}
		return c;
	}

	private double dif(PointF a, PointF b){
		return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
	}

	double dist(PointF pt1, PointF pt2) {
		return Math.sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
	}

	PointF getNoseBase(com.google.android.gms.vision.face.Face face) {
		PointF pos = new PointF();
		for (Landmark lm : face.getLandmarks()) {
			if (lm.getType() == Landmark.NOSE_BASE) {
				pos.x = lm.getPosition().x;
				pos.y = lm.getPosition().y;
			}
		}
		return pos;
	}

	float getrdist(com.google.android.gms.vision.face.Face face) {
		double d = 0;
		int i = 0;
		for (Landmark lm : face.getLandmarks()) {
			if (lm.getType() == Landmark.LEFT_CHEEK || lm.getType() == Landmark.RIGHT_CHEEK) {
				d += dist(lm.getPosition(), getNoseBase(face));
				i++;
			}
		}
		return (float) (d / i);
	}

	PointF rotate_point(PointF c, float degree, PointF p) {
		/*
		double d = Math.toRadians(degree);
		float sin = (float) Math.sin(d);
		float cos = (float) Math.cos(d);
		p.x -= c.x;
		p.y -= c.y;
		// rotate point
		float xnew = p.x * cos - p.y * sin;
		float ynew = p.x * sin + p.y * cos;

		// translate point back:
		p.x = xnew + c.x;
		p.y = ynew + c.y;*/
		return p;
	}

	String landmark(int paramInt) {
		return new String[]{"BOTTOM_MOUTH", "LEFT_CHEEK", "LEFT_EAR_TIP", "LEFT_EAR", "LEFT_EYE", "LEFT_MOUTH", "NOSE_BASE", "RIGHT_CHEEK", "RIGHT_EAR_TIP", "RIGHT_EAR", "RIGHT_EYE", "RIGHT_MOUTH"}[paramInt];
	}

	public static String print(){
		String s = "";
		s = "Match : {Max = "+Maxmatch+", Min = "+MinmatchFloat+", R in range = "+Mcount+",totaltest ="+Tcount+", AVG="+(MAVG/Tcount)+"}";
		s += "Non-Match : {Max = "+NMaxmatch+", Min = "+NMinmatchFloat+", R in range = "+NMcount+",totaltest ="+NTcount+", AVG="+(NAVG/NTcount)+"}";
		return s;
	}
}
