package com.marcuslee.fypdemo.util;

import android.os.AsyncTask;

import com.marcuslee.fypdemo.obj.Face;
import com.marcuslee.fypdemo.obj.Keyframe;
import com.marcuslee.fypdemo.obj.Person;
import com.marcuslee.fypdemo.obj.Shot;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Marcus on 10/20/2015.
 */
public class MetadataParser {
	private static String TAG = MetadataParser.class.getName();
	ObjectHolder OH;

	public MetadataParser() {
		OH = ObjectHolder.getInstance();
	}

	public void ParseData(InputStream is) {
		//InputStream is = getResources().openRawResource(R.raw.a3106818_face);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String readLine = null;
		int i = 0; // 1 = faces, 2= shot , 3 = keyframe

		try {
			while ((readLine = br.readLine()) != null) {
				if (readLine.contains("#FacesId")) {
					i = 1;
				} else if (readLine.contains("#shots\tstart")) {
					i = 2;
				} else if (readLine.contains("#shots\tkeyframe")) {
					i = 3;
				} else {
					switch (i) {
						case 1:
							if (!readLine.isEmpty())
								inputFace(readLine);
							break;
						case 2:
							if (!readLine.isEmpty())
								inputShot(readLine);
							break;
						case 3:
							if (!readLine.isEmpty())
								inputKeyframe(readLine);
							break;
						default:
							break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void inputFace(String str) {
		Person newPerson;
		String[] tstr = str.split("\t", 3);
		if (tstr.length > 2) {
			newPerson = new Person(Integer.parseInt(tstr[0]), tstr[1], tstr[2]);
		} else {
			newPerson = new Person(Integer.parseInt(tstr[0]), tstr[1]);

		}
		ObjectHolder.addPerson(newPerson);
		new GetWebContent().execute(newPerson);
		Logger.debug(TAG, "Loaded Face : ID = " + Integer.parseInt(tstr[0]) + ", Name = " + tstr[1]);
	}

	private void inputShot(String str) {
		String[] tstr = str.split("\t", 3);
		ObjectHolder.addShot(new Shot(Integer.parseInt(tstr[0]), Integer.parseInt(tstr[1]), Integer.parseInt(tstr[2])));
		Logger.debug(TAG, "Loaded Shot : ID = " + Integer.parseInt(tstr[0]) + ", Start frame = " + tstr[1] + ", End frame = " + tstr[2]);
	}

	private void inputKeyframe(String str) {
		String[] tstr = str.split("\t", 7);
		Face face = new Face(Integer.parseInt(tstr[3]), Integer.parseInt(tstr[4]), Integer.parseInt(tstr[5]), Integer.parseInt(tstr[6]));
		face.setPerson(OH.getPerson(Integer.parseInt(tstr[2])));
		Shot shot = OH.getShot(Integer.parseInt(tstr[0]));
		if (shot.getKeyframe() != null) {
			Keyframe Kf = shot.getKeyframe();
			Kf.addFace(face);
		} else {
			Keyframe Kf = new Keyframe(Integer.parseInt(tstr[1]));
			Kf.addFace(face);
			shot.setKeyframe(Kf);
		}
		Logger.debug(TAG, "Loaded Keyframe : Frame = " + Integer.parseInt(tstr[1]) + ", Shot ID = " + Integer.parseInt(tstr[0]) + ", Person = " + Integer.parseInt(tstr[2]) + ", (x,y) = (" + Integer.parseInt(tstr[3]) + ", " + Integer.parseInt(tstr[4]) + "), width = " + Integer.parseInt(tstr[5]) + ", height = " + Integer.parseInt(tstr[6]));
		//OH.addShot(new Shot(Integer.parseInt(tstr[0]),Integer.parseInt(tstr[1]),Integer.parseInt(tstr[2])));
	}

	public class GetWebContent extends AsyncTask<Person, Void, String[]> {
		Person person;

		@Override
		protected String[] doInBackground(Person... persons) {
			person = persons[0];
			String name = person.getName();
			String g = getGoogleA(name);
			String w = getWikiA(name);
			String[] arr = new String[2];
			arr[0] = g;
			arr[1] = w;
			return arr;
		}

		@Override
		protected void onPostExecute(String[] result) {
			Logger.debug(person.getName(), "Result from google : isEmpty() = " + result[0].isEmpty() + ", length = " + result[0].length() + "; Result from wiki : isEmpty() = " + result[1].isEmpty() + ", length = " + result[1].length());
			if (result[1].isEmpty())
				ObjectHolder.getInstance().getPerson(person.getPersonid()).setDescription(result[0]);
			else
				ObjectHolder.getInstance().getPerson(person.getPersonid()).setDescription(result[1]);
		}

		public String getGoogleA(String s) {
			String r = "";
			s = s.replaceAll(" ", "+");
			String web = "https://www.google.co.uk/search?q=" + s;
			r = getContent(web);
			return r;
		}

		public String getWikiA(String s) {
			String r = "";
			s = s.replaceAll(" ", "_");
			String web = "https://en.m.wikipedia.org/wiki/" + s;
			r = getContent(web);
			return description(r);
		}

		public String getContent(String s) {
			String res = "";
			try {
				URL url = new URL(s);
				Logger.debug("getContent", "URL = " + s);
				Document doc = Jsoup.parse(url, 3000);
				Elements divs;
				if (s.contains("google"))
					divs = doc.select(".kno-rdesc span");
				else
					divs = doc.select("#bodyContent p");
				if (divs.size() > 0) { //found
					int i = 0;
					do {
						Element div = divs.get(i++);
						res = div.text();
					} while (res.contains("HIDDEN ERROR"));
				}
				return res;
			} catch (MalformedURLException | java.net.UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "";
		}

		public String description(String s) {
			String r;
			r = s.replaceAll("\\[\\d+\\]", "");
			String[] a = r.split(" ");
			if (a.length > 80) {
				r = "";
				for (int i = 0; i < 80; i++) {
					r += a[i] + " ";
				}
				r += "...";
			}
			return r;
		}
	}
}
