package com.marcuslee.fypdemo.util;

import android.graphics.PointF;

import com.marcuslee.fypdemo.obj.Face;
import com.marcuslee.fypdemo.obj.Keyframe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Marcus on 10/25/2015.
 */
public class FaceTracker {
	private static FaceTracker instance;
	private HashMap<Face, PointF> FaceMap;
	private Keyframe temp;
	private int shotid;

	private FaceTracker() {
		FaceMap = new HashMap<>();
		shotid = 0;
	}

	public static FaceTracker getInstance() {
		if (instance == null) instance = new FaceTracker();
		return instance;
	}

	public void clear() {
		FaceMap.clear();
	}

	public void setKeyFrame(Keyframe kf) {
		temp = kf;
	}

	public HashMap<Face, PointF> track(android.media.FaceDetector.Face[] arr) {
		//TODO : need flag to show the face in arr has been used
		if (arr.length <= 0) return null;

		if (temp != null) { //has keyframe
			int n = arr.length;
			ArrayList<Face> KeyFaces = temp.getFaces();
			PointF MidPoint = new PointF();
			PointF position = new PointF();
			PointF KeyFacesPos = new PointF();
			double Mindist;
			if (FaceMap.isEmpty()) { // no previous position -> new shot, compare with keyframe
				for (Face f : KeyFaces) {
					Mindist = Double.POSITIVE_INFINITY;          //set to inf.
					KeyFacesPos.set(f.getPosX(), f.getPosY());
					for (int i = 0; i < n; i++) {
						if (arr[i] == null) break;
						arr[i].getMidPoint(MidPoint);
						double dist = calDist(KeyFacesPos, MidPoint);
						if (dist < Mindist) {
							Mindist = dist;
							position = MidPoint;
						}
					}
					FaceMap.put(f, position);
				}
			} else { // have previous position, compare with previous position
				Iterator<Map.Entry<Face, PointF>> iter = FaceMap.entrySet().iterator(); //avoid java.util.ConcurrentModificationException
				while (iter.hasNext()) {
					Map.Entry<Face, PointF> entry = iter.next();
					Mindist = Double.POSITIVE_INFINITY;
					KeyFacesPos = entry.getValue();
					for (int i = 0; i < n; i++) {
						if (arr[i] == null) break;
						Logger.debug("Tracking Face #" + i + "/" + arr.length);
						arr[i].getMidPoint(MidPoint);
						double dist = calDist(KeyFacesPos, MidPoint);
						if (dist < Mindist) {
							Mindist = dist;
							position = MidPoint;
						}
					}
					FaceMap.put(entry.getKey(), position);
				}
			}
		} else {
			return null;
		}
		return FaceMap;
	}

	private double calDist(PointF pt1, PointF pt2) {
		float a = pt1.x - pt2.x;
		float b = pt1.y - pt2.y;
		return Math.sqrt(a * a + b * b);
	}

	public int getShotid() {
		return shotid;
	}

	public void setShotid(int shotid) {
		this.shotid = shotid;
	}
}
