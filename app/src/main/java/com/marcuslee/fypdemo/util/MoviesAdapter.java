package com.marcuslee.fypdemo.util;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcuslee.fypdemo.R;
import com.marcuslee.fypdemo.db.obj.Movies;
import com.marcuslee.fypdemo.obj.Movie;

import java.io.File;
import java.util.List;

public class MoviesAdapter extends BaseAdapter {
	private LayoutInflater myInflater;
	private List<Movies> movies;

	public MoviesAdapter(Context context, List<Movies> movie){
		myInflater = LayoutInflater.from(context);
		this.movies = movie;
	}
	@Override
	public int getCount() {
		return movies.size();
	}

	@Override
	public Object getItem(int position) {
		return movies.get(position);
	}
	@Override
	public long getItemId(int position) {
		return movies.indexOf(getItem(position));
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView==null){
			convertView = myInflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder(
					(TextView) convertView.findViewById(R.id.title),
					(TextView) convertView.findViewById(R.id.fullPath),
					(ImageView) convertView.findViewById(R.id.imageView)
			);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		Movies movie = (Movies)getItem(position);
		Uri path = Uri.parse(movie.getFileName());

		String filename = path.getLastPathSegment();
		int ind = filename.lastIndexOf(".");
		filename = filename.substring(0,ind) + ".dat";
		ind = path.toString().lastIndexOf("/");
		filename = path.toString().substring(0,ind)+"/"+filename;
		File f = new File(filename);

		if(!f.exists())holder.image.setVisibility(View.INVISIBLE);
		else holder.image.setVisibility(View.VISIBLE);
		holder.fileName.setText(path.getLastPathSegment());
		holder.filePath.setText(path.toString());
		return convertView;
	}
	private class ViewHolder {
		TextView fileName;
		TextView filePath;
		ImageView image;
		public ViewHolder(TextView fileName, TextView filePath, ImageView image){
			this.fileName = fileName;
			this.filePath = filePath;
			this.image = image;
		}
	}
}