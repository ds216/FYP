package com.marcuslee.fypdemo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.marcuslee.fypdemo.db.obj.DBItem;
import com.marcuslee.fypdemo.db.obj.Persons;
import com.marcuslee.fypdemo.obj.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 2/13/2016.
 */
public class PersonsDAO implements DAO {

	public static final String TABLE_NAME = "persons";
	public static final String KEY_ID = "_id";
	public static final String NAME_COLUMN = "name";

	public static final String CREATE_TABLE =
			"CREATE TABLE " + TABLE_NAME + " (" +
					KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					NAME_COLUMN + " TEXT NOT NULL)";

	private SQLiteDatabase db;

	public PersonsDAO(Context context) {
		db = DBHelper.getDatabase(context);
	}

	@Override
	public void close() {
		db.close();
	}

	@Override
	public DBItem insert(DBItem item) {
		Persons record = (Persons) item;
		ContentValues cv = new ContentValues();
		cv.put(NAME_COLUMN, record.getPerson().getName());
		long id = db.insert(TABLE_NAME, null, cv);
		record.setId(id);
		return record;
	}

	@Override
	public boolean delete(DBItem item) {
		Persons record = (Persons) item;
		return delete(record.getId());
	}

	public boolean delete(long id) {
		String where = KEY_ID + "=" + id;
		return db.delete(TABLE_NAME, where, null) > 0;
	}

	@Override
	public boolean update(DBItem item) {
		return false;
	}

	@Override
	public List<DBItem> getAll() {
		List<DBItem> result = new ArrayList<>();
		Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
		while (cursor.moveToNext()) {
			result.add(getRecord(cursor));
		}
		cursor.close();
		return result;
	}

	@Override
	public Persons get(long id) {
		Persons record = null;
		String where = KEY_ID + "=" + id;
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	public Persons getByPerson(Person person) {
		Persons record = null;
		String where = NAME_COLUMN + "=" + person.getName();
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	public Persons getByPersonName(String person) {
		Persons record = null;
		String where = NAME_COLUMN + "= '" + person + "'";
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	@Override
	public Persons getRecord(Cursor cursor) {
		int i = 0;
		Persons result = new Persons();
		result.setId(cursor.getLong(i++));
		result.setPerson(new Person(cursor.getString(i++)));
		return result;
	}

	@Override
	public int getCount() {
		int result = 0;
		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);
		if (cursor.moveToNext()) {
			result = cursor.getInt(0);
		}
		return result;
	}

	public void createTestData() {
		Persons item1 = new Persons(new Person(1, "Person A"));
		Persons item2 = new Persons(new Person(2, "Person B"));
		Persons item3 = new Persons(new Person(2, "Person C"));
		Persons item4 = new Persons(new Person(2, "Person D"));
		insert(item1);
		insert(item2);
		insert(item3);
		insert(item4);
	}
}
