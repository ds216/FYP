package com.marcuslee.fypdemo.db.obj;

import com.marcuslee.fypdemo.obj.Person;

/**
 * Created by Marcus on 2/12/2016.
 */
public class Persons extends DBItem {
	private long id;
	private Person person;

	public Persons() {
	}

	public Persons(Person person) {
		this.person = person;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String toString() {
		return "#Id = " + id + " : " + "PersonName = " + person.getName();
	}

	public boolean equals(Persons a){
		if(id != a.id) return false;
		if(person.getPersonid() != a.getPerson().getPersonid()) return false;
		if(!person.getName().equals(a.getPerson().getName()))return false;
		return true;
	}
}
