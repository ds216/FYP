package com.marcuslee.fypdemo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PointF;

import com.marcuslee.fypdemo.util.Logger;
import com.marcuslee.fypdemo.db.obj.DBItem;
import com.marcuslee.fypdemo.db.obj.Faces;
import com.marcuslee.fypdemo.obj.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 2/12/2016.
 */
public class FacesDAO implements DAO {

	public static final String TABLE_NAME = "faces";
	public static final String KEY_ID = "_id";
	public static final String NAME_COLUMN = "name";
	public static final String BOTTOM_MOUTH_COLUMN_X = "bottom_mouth_x";
	public static final String LEFT_CHEEK_COLUMN_X = "left_cheek_x";
	public static final String LEFT_EAR_TIP_COLUMN_X = "left_ear_tip_x";
	public static final String LEFT_EAR_COLUMN_X = "left_ear_x";
	public static final String LEFT_EYE_COLUMN_X = "left_eye_x";
	public static final String LEFT_MOUTH_COLUMN_X = "left_mouth_x";
	public static final String NOSE_BASE_COLUMN_X = "nose_base_x";
	public static final String RIGHT_CHEEK_COLUMN_X = "right_cheek_x";
	public static final String RIGHT_EAR_TIP_COLUMN_X = "right_ear_tip_x";
	public static final String RIGHT_EAR_COLUMN_X = "right_ear_x";
	public static final String RIGHT_EYE_COLUMN_X = "right_eye_x";
	public static final String RIGHT_MOUTH_COLUMN_X = "right_mouth_x";

	public static final String BOTTOM_MOUTH_COLUMN_Y = "bottom_mouth_y";
	public static final String LEFT_CHEEK_COLUMN_Y = "left_cheek_y";
	public static final String LEFT_EAR_TIP_COLUMN_Y = "left_ear_tip_y";
	public static final String LEFT_EAR_COLUMN_Y = "left_ear_y";
	public static final String LEFT_EYE_COLUMN_Y = "left_eye_y";
	public static final String LEFT_MOUTH_COLUMN_Y = "left_mouth_y";
	public static final String NOSE_BASE_COLUMN_Y = "nose_base_y";
	public static final String RIGHT_CHEEK_COLUMN_Y = "right_cheek_y";
	public static final String RIGHT_EAR_TIP_COLUMN_Y = "right_ear_tip_y";
	public static final String RIGHT_EAR_COLUMN_Y = "right_ear_y";
	public static final String RIGHT_EYE_COLUMN_Y = "right_eye_y";
	public static final String RIGHT_MOUTH_COLUMN_Y = "right_mouth_y";
	public static final String NUMS_COLUMN = "nums";

	public static final String CREATE_TABLE =
			"CREATE TABLE " + TABLE_NAME + " (" +
					KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					NAME_COLUMN + " TEXT NOT NULL, " +
					BOTTOM_MOUTH_COLUMN_X + " REAL, " +
					BOTTOM_MOUTH_COLUMN_Y + " REAL, " +
					LEFT_CHEEK_COLUMN_X + " REAL, " +
					LEFT_CHEEK_COLUMN_Y + " REAL, " +
					LEFT_EAR_TIP_COLUMN_X + " REAL, " +
					LEFT_EAR_TIP_COLUMN_Y + " REAL, " +
					LEFT_EAR_COLUMN_X + " REAL, " +
					LEFT_EAR_COLUMN_Y + " REAL, " +
					LEFT_EYE_COLUMN_X + " REAL, " +
					LEFT_EYE_COLUMN_Y + " REAL, " +
					LEFT_MOUTH_COLUMN_X + " REAL, " +
					LEFT_MOUTH_COLUMN_Y + " REAL, " +
					NOSE_BASE_COLUMN_X + " REAL, " +
					NOSE_BASE_COLUMN_Y + " REAL, " +
					RIGHT_CHEEK_COLUMN_X + " REAL, " +
					RIGHT_CHEEK_COLUMN_Y + " REAL, " +
					RIGHT_EAR_TIP_COLUMN_X + " REAL, " +
					RIGHT_EAR_TIP_COLUMN_Y + " REAL, " +
					RIGHT_EAR_COLUMN_X + " REAL, " +
					RIGHT_EAR_COLUMN_Y + " REAL, " +
					RIGHT_EYE_COLUMN_X + " REAL, " +
					RIGHT_EYE_COLUMN_Y + " REAL, " +
					RIGHT_MOUTH_COLUMN_X + " REAL, " +
					RIGHT_MOUTH_COLUMN_Y + " REAL, " +
					NUMS_COLUMN + " INTEGER)";

	private SQLiteDatabase db;

	public FacesDAO(Context context) {
		db = DBHelper.getDatabase(context);
	}

	@Override
	public void close() {
		db.close();
	}

	@Override
	public Faces insert(DBItem aRecord) {
		Faces record = (Faces) aRecord;
		ContentValues cv = new ContentValues();
		cv.put(NAME_COLUMN, record.getPerson().getName());
		cv.put(BOTTOM_MOUTH_COLUMN_X, record.getBottom_mouth().x);
		cv.put(LEFT_CHEEK_COLUMN_X, record.getLeft_cheek().x);
		cv.put(LEFT_EAR_TIP_COLUMN_X, record.getLeft_ear_tip().x);
		cv.put(LEFT_EAR_COLUMN_X, record.getLeft_ear().x);
		cv.put(LEFT_EYE_COLUMN_X, record.getLeft_eye().x);
		cv.put(LEFT_MOUTH_COLUMN_X, record.getLeft_mouth().x);
		cv.put(NOSE_BASE_COLUMN_X, record.getNose_base().x);
		cv.put(RIGHT_CHEEK_COLUMN_X, record.getRight_cheek().x);
		cv.put(RIGHT_EAR_TIP_COLUMN_X, record.getRight_ear_tip().x);
		cv.put(RIGHT_EAR_COLUMN_X, record.getRight_ear().x);
		cv.put(RIGHT_EYE_COLUMN_X, record.getRight_eye().x);
		cv.put(RIGHT_MOUTH_COLUMN_X, record.getRight_mouth().x);

		cv.put(BOTTOM_MOUTH_COLUMN_Y, record.getBottom_mouth().y);
		cv.put(LEFT_CHEEK_COLUMN_Y, record.getLeft_cheek().y);
		cv.put(LEFT_EAR_TIP_COLUMN_Y, record.getLeft_ear_tip().y);
		cv.put(LEFT_EAR_COLUMN_Y, record.getLeft_ear().y);
		cv.put(LEFT_EYE_COLUMN_Y, record.getLeft_eye().y);
		cv.put(LEFT_MOUTH_COLUMN_Y, record.getLeft_mouth().y);
		cv.put(NOSE_BASE_COLUMN_Y, record.getNose_base().y);
		cv.put(RIGHT_CHEEK_COLUMN_Y, record.getRight_cheek().y);
		cv.put(RIGHT_EAR_TIP_COLUMN_Y, record.getRight_ear_tip().y);
		cv.put(RIGHT_EAR_COLUMN_Y, record.getRight_ear().y);
		cv.put(RIGHT_EYE_COLUMN_Y, record.getRight_eye().y);
		cv.put(RIGHT_MOUTH_COLUMN_Y, record.getRight_mouth().y);
		cv.put(NUMS_COLUMN, record.getNums());
		long id = db.insert(TABLE_NAME, null, cv);
		record.setId(id);
		return record;
	}

	@Override
	public boolean update(DBItem aRecord) {
		Faces record = (Faces) aRecord;
		Faces oFace = this.getByPerson(record.getPerson());
		ContentValues cv = new ContentValues();
		cv.put(NAME_COLUMN, record.getPerson().getName());
		Logger.debug("record.getBottom_mouth()", record.getBottom_mouth().x + "");
		Logger.debug("oFace", oFace.toString());
		cv.put(BOTTOM_MOUTH_COLUMN_X, (record.getBottom_mouth().x + oFace.getBottom_mouth().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_CHEEK_COLUMN_X, (record.getLeft_cheek().x + oFace.getLeft_cheek().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_EAR_TIP_COLUMN_X, (record.getLeft_ear_tip().x + oFace.getLeft_ear_tip().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_EAR_COLUMN_X, (record.getLeft_ear().x + oFace.getLeft_ear().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_EYE_COLUMN_X, (record.getLeft_eye().x + oFace.getLeft_eye().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_MOUTH_COLUMN_X, (record.getLeft_mouth().x + oFace.getLeft_mouth().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(NOSE_BASE_COLUMN_X, (record.getNose_base().x + oFace.getNose_base().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_CHEEK_COLUMN_X, (record.getRight_cheek().x + oFace.getRight_cheek().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_EAR_TIP_COLUMN_X, (record.getRight_ear_tip().x + oFace.getRight_ear_tip().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_EAR_COLUMN_X, (record.getRight_ear().x + oFace.getRight_ear().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_EYE_COLUMN_X, (record.getRight_eye().x + oFace.getRight_eye().x * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_MOUTH_COLUMN_X, (record.getRight_mouth().x + oFace.getRight_mouth().x * oFace.getNums()) / (oFace.getNums() + 1));

		cv.put(BOTTOM_MOUTH_COLUMN_Y, (record.getBottom_mouth().y + oFace.getBottom_mouth().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_CHEEK_COLUMN_Y, (record.getLeft_cheek().y + oFace.getLeft_cheek().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_EAR_TIP_COLUMN_Y, (record.getLeft_ear_tip().y + oFace.getLeft_ear_tip().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_EAR_COLUMN_Y, (record.getLeft_ear().y + oFace.getLeft_ear().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_EYE_COLUMN_Y, (record.getLeft_eye().y + oFace.getLeft_eye().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(LEFT_MOUTH_COLUMN_Y, (record.getLeft_mouth().y + oFace.getLeft_mouth().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(NOSE_BASE_COLUMN_Y, (record.getNose_base().y + oFace.getNose_base().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_CHEEK_COLUMN_Y, (record.getRight_cheek().y + oFace.getRight_cheek().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_EAR_TIP_COLUMN_Y, (record.getRight_ear_tip().y + oFace.getRight_ear_tip().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_EAR_COLUMN_Y, (record.getRight_ear().y + oFace.getRight_ear().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_EYE_COLUMN_Y, (record.getRight_eye().y + oFace.getRight_eye().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(RIGHT_MOUTH_COLUMN_Y, (record.getRight_mouth().y + oFace.getRight_mouth().y * oFace.getNums()) / (oFace.getNums() + 1));
		cv.put(NUMS_COLUMN, oFace.getNums() + 1);
		String where = KEY_ID + "=" + oFace.getId();
		return db.update(TABLE_NAME, cv, where, null) > 0;
	}

	@Override
	public boolean delete(DBItem aRecord) {
		Faces record = (Faces) aRecord;
		return delete(record.getId());
	}

	public boolean delete(long id) {
		String where = KEY_ID + "=" + id;
		return db.delete(TABLE_NAME, where, null) > 0;
	}

	@Override
	public List<DBItem> getAll() {
		List<DBItem> result = new ArrayList<>();
		Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
		while (cursor.moveToNext()) {
			result.add(getRecord(cursor));
		}
		cursor.close();
		return result;
	}

	@Override
	public Faces get(long id) {
		Faces record = null;
		String where = KEY_ID + "=" + id;
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	@Override
	public Faces getRecord(Cursor cursor) {
		int i = 0;
		Faces result = new Faces();
		result.setId(cursor.getLong(i++));
		result.setPerson(new Person(cursor.getString(i++)));
		PointF newPoint = new PointF();
		result.setBottom_mouth(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setLeft_cheek(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setLeft_ear_tip(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setLeft_ear(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setLeft_eye(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setLeft_mouth(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setNose_base(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setRight_cheek(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setRight_ear_tip(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setRight_ear(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setRight_eye(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setRight_mouth(cursor.getFloat(i++), cursor.getFloat(i++));
		result.setNums(cursor.getInt(i++));
		return result;
	}

	@Override
	public int getCount() {
		int result = 0;
		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);
		if (cursor.moveToNext()) {
			result = cursor.getInt(0);
		}
		return result;
	}

	public Faces getByPerson(Person person) {
		Faces record = null;
		String where = NAME_COLUMN + "='" + person.getName() + "'";
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	public void createTestData() {
		Faces item1 = new Faces(new Person(1, "Person A"), new PointF(1.1f, 1.2f), new PointF(1.3f, 1.4f), new PointF(1.5f, 1.6f), new PointF(1.7f, 1.8f), new PointF(1.9f, 2.1f), new PointF(2.2f, 2.3f), new PointF(2.4f, 2.5f), new PointF(2.6f, 2.7f), new PointF(2.8f, 2.9f), new PointF(3.1f, 3.2f), new PointF(3.3f, 3.4f), new PointF(3.5f, 3.6f), 1);
		Faces item2 = new Faces(new Person(2, "Person B"), new PointF(2.1f, 2.2f), new PointF(2.3f, 2.4f), new PointF(2.5f, 2.6f), new PointF(2.7f, 2.8f), new PointF(2.9f, 3.1f), new PointF(3.2f, 3.3f), new PointF(3.4f, 3.5f), new PointF(3.6f, 3.7f), new PointF(3.8f, 3.9f), new PointF(4.1f, 4.2f), new PointF(4.3f, 4.4f), new PointF(4.5f, 4.6f), 1);
		Faces item3 = new Faces(new Person(3, "Person C"), new PointF(3.1f, 3.2f), new PointF(3.3f, 3.4f), new PointF(3.5f, 3.6f), new PointF(3.7f, 3.8f), new PointF(3.9f, 4.1f), new PointF(4.2f, 4.3f), new PointF(4.4f, 4.5f), new PointF(4.6f, 4.7f), new PointF(4.8f, 4.9f), new PointF(5.1f, 5.2f), new PointF(5.3f, 5.4f), new PointF(5.5f, 5.6f), 1);
		Faces item4 = new Faces(new Person(4, "Person D"), new PointF(4.1f, 4.2f), new PointF(4.3f, 4.4f), new PointF(4.5f, 4.6f), new PointF(4.7f, 4.8f), new PointF(4.9f, 5.1f), new PointF(5.2f, 5.3f), new PointF(5.4f, 5.5f), new PointF(5.6f, 5.7f), new PointF(5.8f, 5.9f), new PointF(6.1f, 6.2f), new PointF(6.3f, 6.4f), new PointF(6.5f, 6.6f), 1);

		insert(item1);
		insert(item2);
		insert(item3);
		insert(item4);
	}
}
