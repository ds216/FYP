package com.marcuslee.fypdemo.db;

import android.database.Cursor;

import com.marcuslee.fypdemo.db.obj.DBItem;

import java.util.List;

/**
 * Created by Marcus on 2/12/2016.
 */
public interface DAO {

	void close();

	DBItem insert(DBItem item);

	boolean delete(DBItem item);

	boolean update(DBItem item);

	List<DBItem> getAll();

	DBItem get(long id);

	DBItem getRecord(Cursor cursor);

	int getCount();
}
