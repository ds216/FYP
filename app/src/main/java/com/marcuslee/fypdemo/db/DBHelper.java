package com.marcuslee.fypdemo.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "facedata.db";
	public static final int VERSION = 13;
	private static SQLiteDatabase database;

	public DBHelper(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}

	public DBHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(FacesDAO.CREATE_TABLE);
		db.execSQL(PersonsDAO.CREATE_TABLE);
		db.execSQL(MoviesDAO.CREATE_TABLE);
		db.execSQL(GroupingDAO.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + FacesDAO.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + PersonsDAO.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + MoviesDAO.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + GroupingDAO.TABLE_NAME);
		onCreate(db);
	}

	public static SQLiteDatabase getDatabase(Context context) {
		if (database == null || !database.isOpen()) {
			database = new DBHelper(context, DATABASE_NAME,
					null, VERSION).getWritableDatabase();
		}
		return database;
	}
}
