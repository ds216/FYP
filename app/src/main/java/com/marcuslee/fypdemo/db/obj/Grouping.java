package com.marcuslee.fypdemo.db.obj;

/**
 * Created by Marcus on 4/4/2016.
 */
public class Grouping extends DBItem {
	private long id;
	private Movies filePath;
	private Persons persons;

	public Grouping(){

	}
	public Grouping(Persons persons, Movies filePath) {
		this.persons = persons;
		this.filePath = filePath;
	}

	public Movies getFilePath() {
		return filePath;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setFilePath(Movies filePath) {
		this.filePath = filePath;
	}

	public Persons getPersons() {
		return persons;
	}

	public void setPersons(Persons persons) {
		this.persons = persons;
	}

	public String toString() {
		return "#Id = " + id + " : " + "fileName = " + filePath + ", persons = " +persons;
	}
}
