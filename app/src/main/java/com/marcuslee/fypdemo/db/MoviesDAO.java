package com.marcuslee.fypdemo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.marcuslee.fypdemo.db.obj.DBItem;
import com.marcuslee.fypdemo.db.obj.Movies;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 2/22/2016.
 */
public class MoviesDAO implements DAO {
	public static final String TABLE_NAME = "movies";
	public static final String KEY_ID = "_id";
	public static final String FILENAME_COLUMN = "fileName";

	public static final String CREATE_TABLE =
			"CREATE TABLE " + TABLE_NAME + " (" +
					KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					FILENAME_COLUMN + " TEXT NOT NULL)";

	private SQLiteDatabase db;

	public MoviesDAO(Context context) {
		this.db = DBHelper.getDatabase(context);
	}

	@Override
	public void close() {
		db.close();
	}

	@Override
	public DBItem insert(DBItem item) {
		Movies record = (Movies) item;
		ContentValues cv = new ContentValues();
		cv.put(FILENAME_COLUMN, record.getFileName());
		long id = db.insert(TABLE_NAME, null, cv);
		record.setId(id);
		return record;
	}

	@Override
	public boolean delete(DBItem item) {
		return false;
	}

	@Override
	public boolean update(DBItem item) {
		return false;
	}

	@Override
	public List<DBItem> getAll() {
		List<DBItem> result = new ArrayList<>();
		Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
		while (cursor.moveToNext()) {
			result.add(getRecord(cursor));
		}
		cursor.close();
		return result;
	}

	@Override
	public DBItem get(long id) {
		Movies record = null;
		String where = KEY_ID + "=" + id;
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	public DBItem getByFilename(String filename) {
		Movies record = null;
		String where = FILENAME_COLUMN + " = '" + filename + "'";
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = getRecord(result);
		}
		result.close();
		return record;
	}

	@Override
	public Movies getRecord(Cursor cursor) {
		int i = 0;
		Movies result = new Movies();
		result.setId(cursor.getLong(i++));
		result.setFileName(cursor.getString(i++));
		return result;
	}

	@Override
	public int getCount() {
		return 0;
	}
}
