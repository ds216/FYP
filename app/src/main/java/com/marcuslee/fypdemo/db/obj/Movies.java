package com.marcuslee.fypdemo.db.obj;

/**
 * Created by Marcus on 2/22/2016.
 */
public class Movies extends DBItem {
	private long id;
	private String fileName;

	public Movies() {

	}

	public Movies(String fileName) {
		this.fileName = fileName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String toString() {
		return "#Id = " + id + " : " + "fileName = " + fileName;
	}
}
