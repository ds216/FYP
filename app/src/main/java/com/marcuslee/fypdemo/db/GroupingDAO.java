package com.marcuslee.fypdemo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.marcuslee.fypdemo.db.obj.DBItem;
import com.marcuslee.fypdemo.db.obj.Grouping;
import com.marcuslee.fypdemo.db.obj.Movies;
import com.marcuslee.fypdemo.db.obj.Persons;

import java.util.ArrayList;
import java.util.List;

public class GroupingDAO implements DAO  {
	public static final String TABLE_NAME = "grouping";
	public static final String KEY_ID = "_id";
	public static final String MOVIE_COLUMN = "moviesId";
	public static final String PERSON_COLUMN = "personsId";

	public static final String CREATE_TABLE =
			"CREATE TABLE " + TABLE_NAME + " (" +
					KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					MOVIE_COLUMN + " INTEGER,"+
					PERSON_COLUMN+" INTEGER)";
	private SQLiteDatabase db;
	private MoviesDAO mDAO;
	private PersonsDAO pDAO;

	public GroupingDAO(Context context) {
		db = DBHelper.getDatabase(context);
		mDAO = new MoviesDAO(context);
		pDAO = new PersonsDAO(context);
	}
	@Override
	public void close() {
		db.close();
	}

	@Override
	public DBItem insert(DBItem item) {
		Grouping record = (Grouping) item;
		ContentValues cv = new ContentValues();
		cv.put(MOVIE_COLUMN, record.getFilePath().getId());
		cv.put(PERSON_COLUMN, record.getPersons().getId());
		long id = db.insert(TABLE_NAME, null, cv);
		record.setId(id);
		return record;
	}

	@Override
	public boolean delete(DBItem item) {
		return false;
	}

	@Override
	public boolean update(DBItem item) {
		return false;
	}

	@Override
	public List<DBItem> getAll() {
		List<DBItem> result = new ArrayList<>();
		Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
		while (cursor.moveToNext()) {
			result.add(getRecord(cursor));
		}
		cursor.close();
		return result;
	}

	@Override
	public DBItem get(long id) {
		Grouping record = null;
		String where = KEY_ID + "=" + id;
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = (Grouping) getRecord(result);
		}
		result.close();
		return record;
	}

	public DBItem get(Grouping item){
		Grouping record = null;
		String where = MOVIE_COLUMN + " = '" + item.getFilePath().getId() + "' AND "+PERSON_COLUMN+ " = '"+item.getPersons().getId()+"'";
		Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
		if (result.moveToFirst()) {
			record = (Grouping)getRecord(result);
		}
		result.close();
		return record;
	}

	@Override
	public DBItem getRecord(Cursor cursor) {
		int i = 0;
		Grouping result = new Grouping();
		result.setId(cursor.getLong(i++));
		Movies m = (Movies) mDAO.get(cursor.getLong(i++));
		if(m==null)return null;
		result.setFilePath(m);
		Persons p = (Persons) pDAO.get(cursor.getLong(i++));
		if(p==null)return null;
		result.setPersons(p);
		return result;
	}

	@Override
	public int getCount() {
		return 0;
	}
}
