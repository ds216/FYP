package com.marcuslee.fypdemo.db.obj;

import android.graphics.PointF;

import com.marcuslee.fypdemo.db.FacesDAO;
import com.marcuslee.fypdemo.obj.Person;

/**
 * Created by Marcus on 2/12/2016.
 */
public class Faces extends DBItem {
	private Person Person;
	private PointF Bottom_mouth = new PointF(0, 0);
	private PointF Left_cheek = new PointF(0, 0);
	private PointF Left_ear_tip = new PointF(0, 0);
	private PointF Left_ear = new PointF(0, 0);
	private PointF Left_eye = new PointF(0, 0);
	private PointF Left_mouth = new PointF(0, 0);
	private PointF Nose_base = new PointF(0, 0);
	private PointF Right_cheek = new PointF(0, 0);
	private PointF Right_ear_tip = new PointF(0, 0);
	private PointF Right_ear = new PointF(0, 0);
	private PointF Right_eye = new PointF(0, 0);
	private PointF Right_mouth = new PointF(0, 0);
	private int Nums = 1;
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Faces() {
	}

	public Faces(Person person, PointF bottom_mouth, PointF left_cheek, PointF left_ear_tip, PointF left_ear, PointF left_eye, PointF left_mouth, PointF nose_base, PointF right_cheek, PointF right_ear_tip, PointF right_ear, PointF right_eye, PointF right_mouth, int nums) {
		Person = person;
		Bottom_mouth = bottom_mouth;
		Left_cheek = left_cheek;
		Left_ear_tip = left_ear_tip;
		Left_ear = left_ear;
		Left_eye = left_eye;
		Left_mouth = left_mouth;
		Nose_base = nose_base;
		Right_cheek = right_cheek;
		Right_ear_tip = right_ear_tip;
		Right_ear = right_ear;
		Right_eye = right_eye;
		Right_mouth = right_mouth;
		Nums = nums;
	}

	public Person getPerson() {
		return Person;
	}

	public void setPerson(Person person) {
		Person = person;
	}

	public PointF getBottom_mouth() {
		return Bottom_mouth;
	}

	public void setBottom_mouth(PointF bottom_mouth) {
		Bottom_mouth = bottom_mouth;
	}

	public void setBottom_mouth(Float x, Float y) {
		Bottom_mouth = new PointF(x, y);
	}

	public PointF getLeft_cheek() {
		return Left_cheek;
	}

	public void setLeft_cheek(PointF left_cheek) {
		Left_cheek = left_cheek;
	}

	public void setLeft_cheek(Float x, Float y) {
		Left_cheek = new PointF(x, y);
	}

	public PointF getLeft_ear_tip() {
		return Left_ear_tip;
	}

	public void setLeft_ear_tip(PointF left_ear_tip) {
		Left_ear_tip = left_ear_tip;
	}

	public PointF getLeft_ear() {
		return Left_ear;
	}

	public void setLeft_ear(PointF left_ear) {
		Left_ear = left_ear;
	}

	public PointF getLeft_eye() {
		return Left_eye;
	}

	public void setLeft_eye(PointF left_eye) {
		Left_eye = left_eye;
	}

	public PointF getLeft_mouth() {
		return Left_mouth;
	}

	public void setLeft_mouth(PointF left_mouth) {
		Left_mouth = left_mouth;
	}

	public PointF getNose_base() {
		return Nose_base;
	}

	public void setNose_base(PointF nose_base) {
		Nose_base = nose_base;
	}

	public PointF getRight_cheek() {
		return Right_cheek;
	}

	public void setRight_cheek(PointF right_cheek) {
		Right_cheek = right_cheek;
	}

	public PointF getRight_ear_tip() {
		return Right_ear_tip;
	}

	public void setRight_ear_tip(PointF right_ear_tip) {
		Right_ear_tip = right_ear_tip;
	}

	public PointF getRight_ear() {
		return Right_ear;
	}

	public void setRight_ear(PointF right_ear) {
		Right_ear = right_ear;
	}

	public PointF getRight_eye() {
		return Right_eye;
	}

	public void setRight_eye(PointF right_eye) {
		Right_eye = right_eye;
	}

	public PointF getRight_mouth() {
		return Right_mouth;
	}

	public void setRight_mouth(PointF right_mouth) {
		Right_mouth = right_mouth;
	}

	public void setLeft_ear_tip(Float x, Float y) {
		Left_ear_tip = new PointF(x, y);
	}

	public void setLeft_ear(Float x, Float y) {
		Left_ear = new PointF(x, y);
	}


	public void setLeft_eye(Float x, Float y) {
		Left_eye = new PointF(x, y);
	}

	public void setLeft_mouth(Float x, Float y) {
		Left_mouth = new PointF(x, y);
	}

	public void setNose_base(Float x, Float y) {
		Nose_base = new PointF(x, y);
	}

	public void setRight_cheek(Float x, Float y) {
		Right_cheek = new PointF(x, y);
	}


	public void setRight_ear_tip(Float x, Float y) {
		Right_ear_tip = new PointF(x, y);
	}


	public void setRight_ear(Float x, Float y) {
		Right_ear = new PointF(x, y);
	}


	public void setRight_eye(Float x, Float y) {
		Right_eye = new PointF(x, y);
	}

	public void setRight_mouth(Float x, Float y) {
		Right_mouth = new PointF(x, y);
	}

	public int getNums() {
		return Nums;
	}

	public void setNums(int nums) {
		Nums = nums;
	}

	public String toString() {
		String str;
		str = "#Id = " + id + " : " +
				FacesDAO.NAME_COLUMN + " = " + Person.getName() + ", " +
				"BOTTOM_MOUTH_COLUMN" + " = (" + Bottom_mouth.x + ", " + Bottom_mouth.y + "), " +
				"LEFT_CHEEK_COLUMN" + " = (" + Left_cheek.x + ", " + Left_cheek.y + "), " +
				"LEFT_EAR_TIP_COLUMN" + " = (" + Left_ear_tip.x + ", " + Left_ear_tip.y + "), " +
				"LEFT_EAR_COLUMN" + " = (" + Left_ear.x + ", " + Left_ear.y + "), " +
				"LEFT_EYE_COLUMN" + " = (" + Left_eye.x + ", " + Left_eye.y + "), " +
				"LEFT_MOUTH_COLUMN" + " = (" + Left_mouth.x + ", " + Left_mouth.y + "), " +
				"NOSE_BASE_COLUMN" + " = (" + Nose_base.x + ", " + Nose_base.y + "), " +
				"RIGHT_CHEEK_COLUMN" + " = (" + Right_cheek.x + ", " + Right_cheek.y + "), " +
				"RIGHT_EAR_TIP_COLUMN" + " = (" + Right_ear_tip.x + ", " + Right_ear_tip.y + "), " +
				"RIGHT_EAR_COLUMN" + " = (" + Right_ear.x + ", " + Right_ear.y + "), " +
				"RIGHT_EYE_COLUMN" + " = (" + Right_eye.x + ", " + Right_eye.y + "), " +
				"RIGHT_MOUTH_COLUMN" + " = (" + Right_mouth.x + ", " + Right_mouth.y + "), " +
				FacesDAO.NUMS_COLUMN + " = " + Nums;
		return str;
		//return "#Id = " + id + " : " + Bottom_mouth + "," + Left_cheek + "," + Left_ear_tip + "," + Left_ear + "," + Left_eye + "," + Left_mouth + "," + Nose_base + "," + Right_cheek + "," + Right_ear_tip + "," + Right_ear + "," + Right_eye + "," + Right_mouth + "," + Nums;
	}
}
