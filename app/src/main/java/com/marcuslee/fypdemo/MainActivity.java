package com.marcuslee.fypdemo;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.marcuslee.fypdemo.db.GroupingDAO;
import com.marcuslee.fypdemo.db.obj.DBItem;
import com.marcuslee.fypdemo.db.obj.Grouping;
import com.marcuslee.fypdemo.db.obj.Movies;
import com.marcuslee.fypdemo.db.obj.Persons;
import com.marcuslee.fypdemo.obj.Movie;
import com.marcuslee.fypdemo.util.Logger;
import com.marcuslee.fypdemo.util.MovieAdapter;
import com.marcuslee.fypdemo.util.MoviesAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity
		implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	private static ArrayList<Movie> movieList;
	private static Persons selected = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);
		movieList = getMovieList();
		mNavigationDrawerFragment = (NavigationDrawerFragment)
				getFragmentManager().findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(
				R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
				.commit();
	}

	public void onSectionAttached(int number) {
		switch (number) {
			case 1:
				mTitle = getString(R.string.title_section1);
				break;
			case 2:
				mTitle = getString(R.string.title_section2);
				break;
			case 3:
				mTitle = getString(R.string.title_section3);
				break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	public ArrayList<Movie> getMovieList() {
		ArrayList<Movie> list = new ArrayList<>();
		String[] projection = new String[]{
				MediaStore.MediaColumns.DISPLAY_NAME
				, MediaStore.MediaColumns.DATE_ADDED
				, MediaStore.MediaColumns.MIME_TYPE
				, MediaStore.MediaColumns.DATA
		};
		Cursor mCur = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				projection, null, null, null
		);
		mCur.moveToFirst();

		while (mCur.isAfterLast() == false) {
			Movie movie = new Movie();
			Uri uri = Uri.parse(mCur.getString(3));
			movie.setPath(uri);
			String npath = uri.getPath();
			int ind = npath.lastIndexOf('/');
			npath = npath.substring(0, ind);
			int index = mCur.getString(0).lastIndexOf('.');
			npath += "/" + mCur.getString(0).substring(0, index) + ".dat";
			File f = new File(npath);
			if (f.exists()) {
				movie.setHasMetadata(true);
			} else {
			}
			list.add(movie);
			mCur.moveToNext();
		}
		return list;
	}

	public static Persons getSelected() {
		return selected;
	}

	public static void setSelected(Persons selected) {
		MainActivity.selected = selected;
		PlaceholderFragment.newInstance(0);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		public PlaceholderFragment() {
		}

		/**
		 * Returns a new instance of this fragment for the given section
		 * number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
								 Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			ListView listView = (ListView) rootView.findViewById(R.id.listView);
			if (selected == null) {
				MovieAdapter adapter = new MovieAdapter(getActivity(), movieList);
				listView.setAdapter(adapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
						Movie item = (Movie) adapter.getItemAtPosition(position);

						Intent intent = new Intent(getActivity(), PlayerActivity.class);
						intent.putExtra("hasMetaData", item.isHasMetadata());
						intent.putExtra("videoPath", item.getPath().toString());
						startActivity(intent);
					}
				});
			}
			else{
				GroupingDAO gDAO = new GroupingDAO(getActivity());
				List<DBItem> list = gDAO.getAll();
				List<Movies> moviesList = new ArrayList<Movies>();
				for(int i = 0; i< list.size(); i++){
					Grouping tmp = (Grouping) list.get(i);
					if(tmp.getPersons().equals(selected))
						moviesList.add(tmp.getFilePath());
				}
				MoviesAdapter adapter = new MoviesAdapter(getActivity(), moviesList);
				listView.setAdapter(adapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
						Movies item = (Movies) adapter.getItemAtPosition(position);

						Intent intent = new Intent(getActivity(), PlayerActivity.class);
						String tmp = item.getFileName();
						int ind = item.getFileName().lastIndexOf(".");
						tmp = tmp.substring(0,ind)+".dat";
						File f = new File(tmp);
						intent.putExtra("hasMetaData", f.exists());
						intent.putExtra("videoPath", item.getFileName());
						startActivity(intent);
					}
				});
			}

			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(
					getArguments().getInt(ARG_SECTION_NUMBER));
		}
	}



}
