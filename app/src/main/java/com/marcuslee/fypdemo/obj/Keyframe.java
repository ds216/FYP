package com.marcuslee.fypdemo.obj;

import java.util.ArrayList;

/**
 * Created by Marcus on 10/4/2015.
 */
public class Keyframe {
	private int framenumber;
	private ArrayList<Face> faces;

	public Keyframe(int f) {
		framenumber = f;
		faces = new ArrayList<Face>();
	}

	public int getFramenumber() {
		return framenumber;
	}

	public void setFramenumber(int framenumber) {
		this.framenumber = framenumber;
	}

	public ArrayList<Face> getFaces() {
		return faces;
	}

	public int countfaces() {
		return faces.size();
	}

	public void setFaces(ArrayList<Face> faces) {
		this.faces = faces;
	}

	public void addFace(Face f) {
		faces.add(f);
	}

}
