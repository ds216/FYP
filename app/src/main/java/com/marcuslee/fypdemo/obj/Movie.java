package com.marcuslee.fypdemo.obj;

import android.net.Uri;

/**
 * Created by Marcus on 3/8/2016.
 */
public class Movie {
	private Uri Path;
	private boolean hasMetadata;

	public Movie() {
	}

	public Movie(Uri path, boolean hasmetadata) {
		Path = path;
		this.hasMetadata = hasmetadata;
	}

	public Uri getPath() {
		return Path;
	}

	public void setPath(Uri path) {
		Path = path;
	}

	public boolean isHasMetadata() {
		return hasMetadata;
	}

	public void setHasMetadata(boolean hasMetadata) {
		this.hasMetadata = hasMetadata;
	}

	@Override
	public String toString() {
		return "Movie : Path = " + Path.getPath() + ", Metadata : " + hasMetadata;
	}
}
