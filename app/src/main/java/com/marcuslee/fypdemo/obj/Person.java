package com.marcuslee.fypdemo.obj;

/**
 * Created by Marcus on 10/20/2015.
 */
public class Person {
	private int Personid;
	private String Name;
	@Deprecated
	private String Description;

	public Person(String name) {
		this.Name = name;
	}

	public Person(int id, String name) {
		this.Personid = id;
		this.Name = name;
	}

	@Deprecated
	public Person(int id, String name, String description) {
		this.Personid = id;
		this.Name = name;
		this.Description = description;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public int getPersonid() {
		return Personid;
	}

	public void setPersonid(int personid) {
		Personid = personid;
	}

	@Deprecated
	public String getDescription() {
		return Description;
	}

	@Deprecated
	public void setDescription(String description) {
		Description = description;
	}


}
