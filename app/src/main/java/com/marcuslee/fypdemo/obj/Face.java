package com.marcuslee.fypdemo.obj;

import android.graphics.PointF;

/**
 * Created by Marcus on 10/4/2015.
 */
public class Face {
	private int posX;
	private int posY;
	private int width;
	private int height;
	private Person person;

	public Face() {

	}

	public Face(int x, int y, int w, int h) {
		posX = x;
		posY = y;
		width = w;
		height = h;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public PointF getMidPoint() {
		float x = posX + width / 2.0f;
		float y = posY + height / 2.0f;
		return new PointF(x, y);
	}
}
