package com.marcuslee.fypdemo.obj;

/**
 * Created by Marcus on 10/20/2015.
 */
public class Shot {
	private int ShotId;
	private int StartFrame;
	private int EndFrame;
	private Keyframe keyframe = null;

	public Shot(int shotId, int startFrame, int endFrame) {
		ShotId = shotId;
		StartFrame = startFrame;
		EndFrame = endFrame;
	}

	public Shot(Keyframe keyframe, int shotId, int startFrame, int endFrame) {
		this.keyframe = keyframe;
		ShotId = shotId;
		StartFrame = startFrame;
		EndFrame = endFrame;
	}

	public int getShotId() {
		return ShotId;
	}

	public void setShotId(int shotId) {
		ShotId = shotId;
	}

	public int getStartFrame() {
		return StartFrame;
	}

	public void setStartFrame(int startFrame) {
		StartFrame = startFrame;
	}

	public int getEndFrame() {
		return EndFrame;
	}

	public void setEndFrame(int endFrame) {
		EndFrame = endFrame;
	}

	public Keyframe getKeyframe() {
		return keyframe;
	}

	public void setKeyframe(Keyframe keyframe) {
		this.keyframe = keyframe;
	}

	public boolean hasKeyframe() {
		return (keyframe != null);
	}
}
